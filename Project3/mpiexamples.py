# from mpi4py import MPI

# comm = MPI.COMM_WORLD
# rank = comm.Get_rank()
# size = comm.Get_size()

# # master process
# if rank == 0:
#     data = {'x': 1, 'y': 2.0}
#     # master process sends data to worker processes by
#     # going through the ranks of all worker processes
#     for i in range(1, size):
#         comm.send(data, dest=i, tag=i)
#         print('Process {} sent data:'.format(rank), data)

# # worker processes
# else:
#     # each worker process receives data from master process
#     data = comm.recv(source=0, tag=rank)
#     print('Process {} received data:'.format(rank), data)


# from mpi4py import MPI
# import numpy as np

# comm = MPI.COMM_WORLD
# rank = comm.Get_rank()
# nprocs = comm.Get_size()

# if rank == 0:
#     data = np.arange(15.0)

#     # determine the size of each sub-task
#     ave, res = divmod(data.size, nprocs)
#     counts = [ave + 1 if p < res else ave for p in range(nprocs)]

#     # determine the starting and ending indices of each sub-task
#     starts = [sum(counts[:p]) for p in range(nprocs)]
#     ends = [sum(counts[:p+1]) for p in range(nprocs)]

#     # converts data into a list of arrays
#     data = [data[starts[p]:ends[p]] for p in range(nprocs)]
# else:
#     data = None

# data = comm.scatter(data, root=0)

# print('Process {} has data:'.format(rank), data)

from mpi4py import MPI
import time
import math

t0 = time.time()

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprocs = comm.Get_size()

# number of integration steps
nsteps = 10000000
# step size
dx = 1.0 / nsteps

if rank == 0:
    # determine the size of each sub-task
    ave, res = divmod(nsteps, nprocs)
    counts = [ave + 1 if p < res else ave for p in range(nprocs)]

    # determine the starting and ending indices of each sub-task
    starts = [sum(counts[:p]) for p in range(nprocs)]
    ends = [sum(counts[:p+1]) for p in range(nprocs)]

    # save the starting and ending indices in data
    data = [(starts[p], ends[p]) for p in range(nprocs)]
else:
    data = None

data = comm.scatter(data, root=0)

# compute partial contribution to pi on each process
partial_pi = 0.0
for i in range(data[0], data[1]):
    x = (i + 0.5) * dx
    partial_pi += 4.0 / (1.0 + x * x)
partial_pi *= dx

partial_pi = comm.gather(partial_pi, root=0)

if rank == 0:
    print('pi computed in {:.3f} sec'.format(time.time() - t0))
    print('error is {}'.format(abs(sum(partial_pi) - math.pi)))
