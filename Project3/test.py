from scipy.sparse import csr_matrix,csc_matrix
import numpy as np
from scipy.sparse import diags
from scipy.sparse import coo_matrix, vstack
from scipy . sparse . linalg import spsolve , cg
from scipy.ndimage import shift
import matplotlib.pyplot as plt


class border():

    # temperature coefficient
    #shared_vec with length N. equal 1 if shared, otherwise 0
    # the temperature in vec_boarder[i] if not shared is temperature

    def __init__(self,temperature,N,shared_vec=None,initialTemp=0,shared=False) -> None:
        self.temperature=temperature
        self.shared=shared
       
        self.shared_vec=shared_vec
        self.N=N
        N=N
        self.temperature_array=np.zeros(N+1)
        
        for i in range(N+1):
            if shared:
                if self.shared_vec[i] == 1:
                    self.temperature_array[i]= initialTemp
                else:
                    self.temperature_array[i]= initialTemp
            else:
               self.temperature_array[i]=temperature

    def setTemp(self,temperature_vec):
          for i in range(self.N):
            if self.shared_vec[i] == 1:
               self.temperature_array[i]= temperature_vec[i]

    def setTemp_shared(self,temp_vec,start,stop):
        #print("_*****")
        #print(temp_vec)
        #print(  self.temperature_array)
        w=0.8
        self.temperature_array[start:stop]=temp_vec*w +(1-w)*self.temperature_array[start:stop]


        


class Omega():

    def __init__(self,lengthX,lengthY,Nx,Ny,h) -> None:
        self.h=h

        self.Nx=Nx  # ska vara lengthX*h
        self.Ny=Ny  # ska vara LengthY*h
 
        self.y=np.zeros((self.Nx-1)*(self.Ny-1))
        


    def addBoarders(self,border_top,border_bottom,border_left,border_right):

        self.border_top=border_top
        self.border_bottom=border_bottom
        self.border_left=border_left
        self.border_right=border_right


    def pos_is_in(self,N,Nx,Ny):
        i,j =self.getCord(N)     
        if j==0:
            return False
    
        if j==Ny:
              return False

        if i==0:
          return False
        if i==Nx:
            return False
        return True

    def Neumann(self,start,stop,wall:border,WallSide):
        N=wall.N
        h=self.h # ??
        width =  (self.Nx+1)*(self.Nx+1)
        zero=[0.0]*(self.Nx-1)
        
        res=np.zeros(stop+1)
        
        if WallSide=="L":
            param=1
            for i in range(start,stop):
                    res[i]= - (1/h) * (  -3* self.grid[i,param]+self.grid[i+1,param]+self.grid[i-1,param]+self.grid[i,param+1]     )
            res[start-1]=res[start]
            res[stop]=res[stop-1]

        if WallSide == "R":
            param=self.Nx-1
            for i in range(start,stop):
                    res[i]= - (1/h) * (  -3* self.grid[i,param]+self.grid[i+1,param]+self.grid[i-1,param]+self.grid[i,param-1]     )
            res[start-1]=res[start]
            res[stop]=res[stop-1]

        if WallSide=="T":
            param=1
            for i in range(start,stop):
                    #print("_____T_____")
                    #print(self.grid[param,i],self.grid[param,i+1],self.grid[param,i-1],self.grid[param-1,i])



                    res[i]= -(1/h) * (  -3* self.grid[param,i]+self.grid[param,i+1]+self.grid[param,i-1]+self.grid[param+1,i]     )
            res[start-1]=res[start]
            res[stop]=res[stop-1]

        if WallSide=="B":
            param=self.Ny-1
            #print("________B________________")
            #print(self.grid)
            for i in range(start,stop):
                   # print("Nedan")
             #       print(self.grid[param,i],self.grid[param,i+1],self.grid[param,i-1],self.grid[param-1,i])
                    res[i]= -(1/h)*(  -3* self.grid[param,i]+self.grid[param,i+1]+self.grid[param,i-1]+self.grid[param-1,i]     )
            res[start-1]=res[start]
            res[stop]=res[stop-1]

        return res


    def dirichlet(self,start,stop,wall:border,WallSide):
        N=wall.N
         # ??
        width =  (self.Nx+1)*(self.Nx+1)
        zero=[0.0]*(self.Nx-1)
        
        res=np.zeros(stop+1)
        if WallSide=="L":
            param=2
            for i in range(start,stop):
                    res[i]=-1*(  -4* self.grid[i,param]+self.grid[i+1,param]+self.grid[i-1,param]+self.grid[i,param+1]     )
            res[start-1]=res[start]
            res[stop]=res[stop-1]
        if WallSide == "R":
            param=self.Nx-2
            for i in range(start,stop):
                    res[i]=-1*(  -4* self.grid[i,param]+self.grid[i+1,param]+self.grid[i-1,param]+self.grid[i,param-1]     )
            res[start-1]=res[start]
            res[stop]=res[stop-1]
        if WallSide=="T":
            param=1
            for i in range(start,stop):
                    res[i]= -1 * (  -4* self.grid[param,i]+self.grid[param,i+1]+self.grid[param,i-1]+self.grid[param-1,i]     )
            res[start-1]=res[start]
            res[stop]=res[stop-1]

        if WallSide=="B":
            param=self.Ny-1
            #parm=self.Ny-1
            for i in range(start,stop):
                   
                    res[i]=-1.0 * (  -4.0* self.grid[param,i]+self.grid[param,i+1]+self.grid[param,i-1]+self.grid[param+1,i]     )
            res[start-1]=res[start]
            res[stop]=res[stop-1]
        return res


    def createSparse(self):
      
        #width =  (self.Nx+1)*(self.Nx+1)-2
        
        #zero=[0.0]*(self.Nx-1)
        #diag=[1.0]+zero+[1.0,-4.0,1.0]+zero+[1.0]

        self.y=np.zeros((self.Nx-1)*(self.Ny-1))
        Nx=self.Nx
        Ny=self.Ny
        width =  (Nx+1)*(Nx+1)-2
        
        zero=[0.0]*(Nx-1)
        diag=[1.0]+zero+[1.0,-4.0,1.0]+zero+[1.0]
        
        r= np.zeros(  ( (Nx-1)*(Ny-1),width)) 
        last_index=0
        for i in range((Nx-1)*(Ny-1)):
            for j in range(last_index,width-len(diag)+1):
                fut=Nx+2
            
                if self.pos_is_in(fut+j,Nx,Ny):
                 
                    r[i][j:(j+len(diag))]=diag
                    last_index=j+1
                    break



    
        
        
        # offset=np.arange(len(diag))
      
        #self.room=diags(diag,offset,shape=((self.Nx-1)*(self.Ny-1),width)) 
        #print(self.room)

        #t=self.room.toarray()
        #print(t)
       # t[2]=np.roll(t[2],2 )
       # t[3]=np.roll(t[3],2 )
        #t[2]=np.roll(t[2],2 )
        #t[3]=np.roll(t[3],2 )
        self.room=csr_matrix(r)
       



        tmp=self.getKnownV()

            
        for i in range(0,len(tmp)):
            position,value =tmp[i]
            self.y=np.append(self.y, value)
            B = coo_matrix( ([1.0], ([0],[position-1])),shape=(1,width) )
            self.room=vstack([self.room,B] )      
            
        #print(self.y)
        #print(self.room.toarray())

        
        #print(tmp.toarray())
    

    def Solve_sys(self):
        k= np.array(self.room.toarray())
  
        tmp=csr_matrix(np.array(self.y))
    
        X = spsolve( csr_matrix( self.room),((self.y)))


        #sol= np.linalg.solve(self.room.toarray(),self.y  ) 

        #print(X)
        solultion_a=X
        temp=self.border_top.temperature_array[self.border_top.N-1] 
        solultion_a=np.append(solultion_a,temp)
        solultion_a=np.insert(solultion_a,0,self.border_bottom.temperature_array[0] )
        


        solultion_a=np.flipud(solultion_a)
        solultion_a=np.reshape(solultion_a,(self.Nx+1,self.Ny+1))
        
        self.grid=np.flip(solultion_a,1)
       




        return self.grid



    # return an array containing the known Vs as touple  (position number, value)
    def getKnownV(self):
        tmp=[]
        top_t=self.border_bottom.temperature_array
        for i in range(1, len(top_t)):
            res = self.getNbr(0,i)
            tmp.append(   (  res,top_t[i])  )
        top_t=self.border_left.temperature_array
        for i in range(1,len(top_t)-1):
            res = self.getNbr(i,0)
            tmp.append(   (  res,top_t[i])  )
        top_t=self.border_right.temperature_array
        for i in range(1,len(top_t)-1):
            res = self.getNbr(i,self.Ny)
            tmp.append(   (  res,top_t[i])  )
        top_t=self.border_top.temperature_array
        for i in range(0,len(top_t)-1):
            res = self.getNbr(self.Nx,i)
            tmp.append(   ( res,top_t[i])  )

        return tmp


    def getNbr(self,i,j):
        return i*(self.Nx+1)+j

    def getCord(self,N):
        row = N // (1+self.Nx)
        col = N % (1+self.Nx)
        return (row,col)



#test(100)

def test(N):
    O1=Omega(1,1,N,N,1.0/N)
    O2=Omega(1,1,N,N,1.0/N)
    O3=Omega(1,1,N,N,1.0/N)
    O4=Omega(1,1,N,N,1.0/N)


    T1=border(40.0,N)
    shared=border(20.0,N)
    L1=border(15.0,N)
    R_shared=border(20.0,N)


    B2=border(5.0,N)
    L_shared=border(20.0,N)
    R2=border(15.0,N)

    #R3=border(20.0,N)
    T3=border(15.0,N)
    B3=border(15.0,N)
    L3=border(40.0,N)

    T4=border(15.0,N)
    B4=border(15.0,N)
    R4=border(40.0,N)
    

    



    for i in range(40):
        O1.addBoarders(T1,shared,L1,R_shared)
        O1.createSparse()
        solv=O1.Solve_sys()
        res=O1.dirichlet(1,N,shared,"B")
        #print(res)
        #print(shared.temperature_array)
        shared.setTemp_shared(res[0:N],0,N)
        #print(shared.temperature_array)
        O2.addBoarders(shared,B2,L_shared,R2)
        O2.createSparse()
        solv2=O2.Solve_sys()
        res2=O2.dirichlet(1,N,shared,"T")
       
        shared.setTemp_shared(res2[0:N],0,N)
      ##      för vänster ner

        O3.addBoarders(T3,B3,L3,L_shared)
        O3.createSparse()
        solv3=O3.Solve_sys()
        res=O3.dirichlet(1,N,L_shared,"R")
        L_shared.setTemp_shared(res[0:N],0,N)

        O4.addBoarders(T4,B4,R_shared,R4)
        O4.createSparse()
        solv4=O4.Solve_sys()
        res=O4.dirichlet(1,N,R_shared,"L")

        R_shared.setTemp_shared(res[0:N],0,N)


        #O2.addBoarders(shared,B2,L2,R2)
        #O2.createSparse()
        #solv2=O2.Solve_sys()
        #res2=O2.dirichlet(1,N,shared,"T")
        #shared.setTemp_shared(res2[1:N],1,N)
      ##      för vänster ner



        # print(res)
        #print(res2)
        #print()
    solv1=O1.Solve_sys()

    #kk=np.stack(np.array(solv1),np.array(solv2))


    kk1=np.concatenate(( solv4,solv4*0,))
    kk2=np.concatenate((solv1, solv2))
    kk3=np.concatenate(( solv3*0,solv3))

    kkk4=np.concatenate((kk3, kk2), axis=1)
    kkk5=np.concatenate((kkk4, kk1), axis=1)

    plt.imshow(kkk5, cmap='hot', interpolation='none')
    plt.show()


test(100)







def test2(N):
    p=Omega(3,3,N,N,3)
    T=border(15.0,N)
    B=border(75.0,N)
    L=border(50.0,N)
    R=border(20.0,N)
    p.addBoarders(T,B,L,R)
    p.createSparse()

    solultion_a=p.Solve_sys()


    print()
    print(solultion_a)
    print()

    plt.imshow(solultion_a, cmap='hot', interpolation='none')
    plt.show()






def sol3():
    lol = np.array([
    [4.0 , -1.0 , -1 , 0 ],
    [-1 , 4 , 0 , -1],
    [0, -1.0 ,-1, 4.0],
    [-1,0, 4.0 ,-1]
    ])

    lolp=np.array([15.0 , 30.0 , 35.0 , 20.0])

    print(np.linalg.solve(lol,lolp ) )