from scipy.sparse import csr_matrix
import numpy as np
from scipy.sparse import coo_matrix, vstack
from scipy . sparse . linalg import spsolve
import matplotlib.pyplot as plt
from mpi4py import MPI


class border():

    # temperature coefficient
    # shared_vec with length N. equal 1 if shared, otherwise 0
    # the temperature in vec_boarder[i] if not shared is temperature

    def __init__(self, temperature, N, shared_vec=None, initialTemp=0, shared=False) -> None:
        self.temperature = temperature
        self.shared = shared
        self.shared_vec = shared_vec
        self.N = N
        self.temperature_array = np.zeros(N+1)

        for i in range(N+1):
            if shared:
                if self.shared_vec[i] == 1:
                    self.temperature_array[i] = initialTemp
                else:
                    self.temperature_array[i] = initialTemp
            else:
                self.temperature_array[i] = temperature
                

    def setTemp(self, temperature_vec):
        for i in range(self.N):
            if self.shared_vec[i] == 1:
                self.temperature_array[i] = temperature_vec[i]
                

    def setTemp_shared(self, temp_vec, start, stop):
        w = 0.8
        self.temperature_array[start:stop] = temp_vec * \
            w + (1-w)*self.temperature_array[start:stop]


class Omega():
    def __init__(self, lengthX, lengthY, Nx, Ny, h) -> None:
        self.h = h
        self.Nx = Nx  # ska vara lengthX*h
        self.Ny = Ny  # ska vara LengthY*h
        self.y = np.zeros((self.Nx-1)*(self.Ny-1))


    def addBoarders(self, border_top, border_bottom, border_left, border_right):
        self.border_top = border_top
        self.border_bottom = border_bottom
        self.border_left = border_left
        self.border_right = border_right


    def pos_is_in(self, N, Nx, Ny):
        i, j = self.getCord(N)
        
        if j == 0:
            return False
        if j == Ny:
            return False
        if i == 0:
            return False
        if i == Nx:
            return False
        return True

    
    def isBoarder(self,direction,index):
        row,col = self.getCord(index)
        #print(index,row,col,row==1)
        if direction=="L":
            return col==1
        elif direction=="R":
            return col==self.Nx-1
        elif direction=="T":
            return row==self.Ny-1
        elif direction=="B":
            return row==1

        return True


    def createSparse2(self,BondWall):
        self.y = np.zeros((self.Nx-1)*(self.Ny-1))
        Nx = self.Nx
        Ny = self.Ny
        width = (Nx+1)*(Nx+1)-2
        h=1/(Nx)
        zero = [0.0]*(Nx-1)
        diag = [1.0]+zero+[1.0, -4.0, 1.0]+zero+[1.0]   ## -4 -> -3  

        h1,h2,h3,h4=1,1,1,1
        if BondWall=="L":
            h4=h   
        elif BondWall=="R":
            h2=h
        elif BondWall=="T":
            h1=h
        elif BondWall=="B":
            h3=h
            
        diag2 = [1.0*h3]+zero+[1.0*h4, -3.0, 1.0*h2]+zero+[1.0*h1]
        r = np.zeros(((Nx-1)*(Ny-1), width))
        last_index = 0
        
        for i in range((Nx-1)*(Ny-1)):
            for j in range(last_index, width-len(diag)+1):
                fut = Nx+2

                if self.pos_is_in(fut+j, Nx, Ny):
                    if self.isBoarder(BondWall,fut+j):
                        r[i][j:(j+len(diag))]=diag2 
                        last_index = j+1
                        break
                        
                    r[i][j:(j+len(diag))] = diag
                    last_index = j+1
                    break
                
        self.room = csr_matrix(r)
        tmp = self.getKnownV()

        for i in range(0, len(tmp)):
            position, value = tmp[i]
            self.y = np.append(self.y, value)
            B = coo_matrix(([1.0], ([0], [position-1])), shape=(1, width))
            self.room = vstack([self.room, B])


    def createSparse(self):
        self.y = np.zeros((self.Nx-1)*(self.Ny-1))
        Nx = self.Nx
        Ny = self.Ny
        width = (Nx+1)*(Nx+1)-2

        zero = [0.0]*(Nx-1)
        diag = [1.0]+zero+[1.0, -4.0, 1.0]+zero+[1.0]   ## -4 -> -3     

        r = np.zeros(((Nx-1)*(Ny-1), width))
        last_index = 0
        for i in range((Nx-1)*(Ny-1)):
            for j in range(last_index, width-len(diag)+1):
                fut = Nx+2

                if self.pos_is_in(fut+j, Nx, Ny):

                    r[i][j:(j+len(diag))] = diag
                    last_index = j+1
                    break

        self.room = csr_matrix(r)
        tmp = self.getKnownV()

        for i in range(0, len(tmp)):
            position, value = tmp[i]
            self.y = np.append(self.y, value)
            B = coo_matrix(([1.0], ([0], [position-1])), shape=(1, width))
            self.room = vstack([self.room, B])


    def Solve_sys(self,type,side=None):
        self.room=None
        if type=="N":
            self.createSparse2(side)
        else:
            self.createSparse()
        
        k = np.array(self.room.toarray())
        tmp = csr_matrix(np.array(self.y))
        X = spsolve(csr_matrix(self.room), ((self.y)))

        solultion_a = X
        temp = self.border_top.temperature_array[self.border_top.N-1]
        solultion_a = np.append(solultion_a, temp)
        solultion_a = np.insert(
            solultion_a, 0, self.border_bottom.temperature_array[0])

        solultion_a = np.flipud(solultion_a)
        solultion_a = np.reshape(solultion_a, (self.Nx+1, self.Ny+1))

        self.grid = np.flip(solultion_a, 1)

        return self.grid


    # return an array containing the known Vs as touple  (position number, value)
    def getKnownV(self):
        tmp = []
        top_t = self.border_bottom.temperature_array
        for i in range(1, len(top_t)):
            res = self.getNbr(0, i)
            tmp.append((res, top_t[i]))
        top_t = self.border_left.temperature_array
        for i in range(1, len(top_t)-1):
            res = self.getNbr(i, 0)
            tmp.append((res, top_t[i]))
        top_t = self.border_right.temperature_array
        for i in range(1, len(top_t)-1):
            res = self.getNbr(i, self.Ny)
            tmp.append((res, top_t[i]))
        top_t = self.border_top.temperature_array
        for i in range(0, len(top_t)-1):
            res = self.getNbr(self.Nx, i)
            tmp.append((res, top_t[i]))
        return tmp

    def getNbr(self, i, j):
        return i*(self.Nx+1)+j

    def getCord(self, N):
        row = N // (1+self.Nx)
        col = N % (1+self.Nx)
        return (row, col)


## hjälpklass för parallelliseringen. styckena som kan köras var för sig är egna metoder etc
class alg:
    def __init__(self,N):
        self.N = N
        self.Omega2a=Omega(1,1,N,N,1.0/N)
        self.Omega2b=Omega(1,1,N,N,1.0/N)
        self.Omega1=Omega(1,1,N,N,1.0/N)
        self.Omega3=Omega(1,1,N,N,1.0/N)
        self.Omega4=Omega(1,1,N//2,N//2, 4/N)
        
        self.Bottom4=border(40,N//2)
        self.Right4=border(15,N//2)
        self.Top4,self.Left4=border(15.0,N//2),border(15.0,N//2)

        self.Top2a,self.left2a=border(40.0,N),border(15.0,N)
        self.Right2a_Left3=border(15,N)
        self.Bottom2a_Top2b=border(15.0,N)

        self.Right2b,self.Bottom2b=border(15.0,N),border(5.0,N)
        self.Left2b_Right1=border(15.0,N)

        self.Right3,self.Top3,self.Bottom3=border(40.0,N),border(15.0,N),border(15.0,N)
        self.Top1,self.Bottom1,self.Left1=border(15.0,N),border(15.0,N),border(40.0,N)
            
    ## måste köras sekventiellt med par 2 - 4        
    def par1(self):
        self.Omega2a.addBoarders(self.Top2a,self.Bottom2a_Top2b,self.left2a,self.Right2a_Left3)
        self.Omega2a.Solve_sys("D")        
        self.res=self.Omega2a.grid[-2, 1:-1] 
        self.Bottom2a_Top2b.setTemp_shared(self.res,1,self.N)
        self.Omega2b.addBoarders(self.Bottom2a_Top2b,self.Bottom2b,self.Left2b_Right1,self.Right2b)
        self.Omega2b.Solve_sys("D")
        self.res2=self.Omega2b.grid[1, 1:-1]
        self.Bottom2a_Top2b.setTemp_shared(self.res2,1,self.N)

        self.res4=self.Omega2a.grid[1:-1, -2][::-1]
        self.res5=self.Omega2b.grid[1:-1, 1][::-1]
        self.left_for_4=self.Omega2b.grid[1:-1, -2][::-1][1*self.N//2:self.N]
        self.Right2b.setTemp_shared(self.left_for_4,1*self.N//2,self.N-1)
        self.Right2a_Left3.setTemp_shared(self.res4,1,self.N)
        self.Left2b_Right1.setTemp_shared(self.res5,1,self.N)
        self.Left4.setTemp_shared(self.left_for_4,1,self.N//2)
            
    ## delar 2 - 4 kan köras parallellt med varann. 
    def par2(self):
        self.Omega3.addBoarders(self.Top3,self.Bottom3,self.Right2a_Left3,self.Right3)
        self.Omega3.Solve_sys("N","R")
        temp=self.Omega3.grid[1:-1, 1][::-1]
        self.Right2a_Left3.setTemp_shared(temp,1,self.N)
                
    def par3(self):
        self.Omega1.addBoarders(self.Top1,self.Bottom1,self.Left1,self.Left2b_Right1)
        self.Omega1.Solve_sys("N","L")      
        temp = self.Omega1.grid[1:-1, -2][::-1]
        self.Left2b_Right1.setTemp_shared(temp,1,self.N)
        
    def par4(self):
        self.Omega4.addBoarders(self.Top4,self.Bottom4,self.Left4,self.Right4)
        self.Omega4.Solve_sys("N","R")
        self.Right2b.setTemp_shared(self.Omega4.grid[1:-1, 1][::-1],self.N//2+1,self.N)
            
    ## kör algoritmen en sista gång och plottar
    def fin(self):
        ## jämna ut gränserna..
        self.Omega2a.addBoarders(self.Top2a,self.Bottom2a_Top2b,self.left2a,self.Right2a_Left3)
        self.Omega2b.addBoarders(self.Bottom2a_Top2b,self.Bottom2b,self.Left2b_Right1,self.Right2b)
        self.solv_2b=self.Omega2b.Solve_sys("D")
        self.solv_2a=self.Omega2a.Solve_sys("D")
        self.res4=self.Omega2a.grid[1:-1, -2][::-1]
        self.res5=self.Omega2b.grid[1:-1, 2][::-1]
        self.left_for_4=self.Omega2b.grid[1:-1, -2][::-1][1*self.N//2:self.N]
        self.Right2a_Left3.setTemp_shared(self.res4,1,self.N)
        self.Left2b_Right1.setTemp_shared(self.res5,1,self.N)
        self.Left4.setTemp_shared(self.left_for_4,1,self.N//2)    
        self.Right2b.setTemp_shared(self.left_for_4,self.N//2+1,self.N)
        self.Omega2a.addBoarders(self.Top2a,self.Bottom2a_Top2b,self.left2a,self.Right2a_Left3)
        self.Omega2b.addBoarders(self.Bottom2a_Top2b,self.Bottom2b,self.Left2b_Right1,self.Right2b)
        self.solv_2b=self.Omega2b.Solve_sys("D")
        self.solv_2a=self.Omega2a.Solve_sys("D")
        self.Omega4.addBoarders(self.Top4,self.Bottom4,self.Left4,self.Right4)
        self.solv_4=self.Omega4.Solve_sys("D","R")
        self.Omega1.addBoarders(self.Top1,self.Bottom1,self.Left1,self.Left2b_Right1)
        self.solv_1=self.Omega1.Solve_sys("D","R")
        self.Omega3.addBoarders(self.Top3,self.Bottom3,self.Right2a_Left3,self.Right3)
        self.solv_2b=self.Omega2b.Solve_sys("D")     
        self.solv_3=self.Omega3.Solve_sys("D","L")
        self.Omega4.addBoarders(self.Top4,self.Bottom4,self.Left4,self.Right4)
        self.solv_4=self.Omega4.Solve_sys("D","R")
        
        ## plotta
        concat4=np.concatenate(( self.solv_4,self.solv_4*0,))
        concat4b=np.concatenate(( self.solv_4*0,self.solv_4*0,))
        concat4=  np.delete(np.delete(np.concatenate(( concat4,concat4b),axis=1),-1, 0),-1,1)
        t1=np.concatenate(( self.solv_3,concat4,))
        t2=np.concatenate((self.solv_2a, self.solv_2b))
        t3=np.concatenate(( self.solv_1*0,self.solv_1))
        t4 = np.concatenate((t3, t2), axis=1)
        t5 = np.concatenate((t4, t1), axis=1)

        plt.imshow(t5, cmap='inferno', interpolation='none')
        plt.title("FRÅN TEST")
        plt.show()
 
def test(N):
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    f69 = alg(N)

    for i in range(20):
        if rank == 0:
            f69.par1()     ## måste köras sekventiellt med par 2 - 4. vill man va jätteduktig skulle man kunna köra par1 för steg n+1 medan par 2-4 körs för steg n...
            comm.send(f69, dest = 1)
            comm.send(f69, dest = 2)
            
            f69.par2()
            t1 = comm.recv(source = 2)     
            t2 = comm.recv(source = 1)
            f69.Left2b_Right1 = t2.Left2b_Right1   ## plockar in resultaten från de andra trådarna
            f69.Right2b = t1.Right2b
            if i == 19 : f69.fin()    ## ska köras en gång på slutet
        elif rank == 1:
            f69 = comm.recv(source = 0)   ## startar par3 när par1 är klar
            f69.par3()
            comm.send(f69, dest = 0)    ## skickar tillbaka f69 till "master"-tråden
        elif rank == 2:
            f69 = comm.recv(source = 0)   ## likn. som för par3 ovan
            f69.par4()
            comm.send(f69, dest = 0)
                

test(50)   ## argumentet till test är N, med h = 1 / N...
