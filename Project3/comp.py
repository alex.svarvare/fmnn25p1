from mpi4py import MPI
import numpy as np
import matplotlib.pyplot as plt

class Comp:
    def __init__(self, size, h, boundary, boundCond):
        self.size = size #(Nx, Ny)
        self.Nx = self.size[0]
        self.Ny = self.size[1]
        self.h = h
        self.boundary = boundary
        self.boundCond = boundCond

    def calc(self):
        nbrOfInner = (self.Nx - 2) * (self.Ny - 2) # Ax=b
        A = np.zeros([nbrOfInner, nbrOfInner])
        b = np.zeros(nbrOfInner)

        offsets = [np.array([1, 0]), np.array([0, -1]), np.array([-1, 0]), np.array([0, 1])]
        #print(offsets[0])
        #print(offsets[1])
        #print(offsets[2])
        #print(offsets[3])
        for i in range(nbrOfInner):
            t = Comp.tupleIndex(self.size, True, i)
            mul = -4
            for s in offsets:
                shift = tuple(t + s)
                if Comp.isOnEdge(self.Nx, self.Ny, shift):
                    edgeIndex = Comp.edgeIndex(self.Nx, self.Ny,  shift)
                    if self.boundCond[edgeIndex]:
                        # diri bound cond
                        b[i] -= self.boundary[edgeIndex]
                    else:
                        # neuermann bound cond
                        b[i] -= self.boundary[edgeIndex] * self.h
                        mul += 1
                else:
                    i_shift = Comp.tupleIndex(self.size, True, shift)
                    A[i, i_shift] = 1

            A[i, i] = mul #Set the diagonal

        x = np.linalg.solve(A, b)

        sol = np.zeros(self.size)
        for i in range(len(self.boundary)):
            t = Comp.edgeIndex(self.Nx, self.Ny,  i)
            if self.boundCond[i]:
                sol[t] = self.boundary[i]
            else:
                bound = Comp.isOnEdge(self.Nx, self.Ny, t)
                s = offsets[bound - 1]
                shiftV2 = tuple(t + s)
                i_shift = Comp.tupleIndex(self.size, True, shiftV2)
                if bound in [2, 3]:
                    sol[t] = x[i_shift] + self.h * self.boundary[i]
                else:
                    sol[t] = x[i_shift] + self.h * self.boundary[i]

        for i in range(nbrOfInner):
            t = Comp.tupleIndex(self.size, True, i)
            sol[t] = x[i]

        return sol


    def tupleIndex(grid_size, without_border, i):
        if type(i) is int:
            if without_border:
                return i // (grid_size[1] - 2) + 1, i % (grid_size[1] - 2) + 1
            else:

                return i // grid_size[1], i % grid_size[1]
        elif type(i) is tuple and len(i) == 2:
            if without_border:
                return (i[0] - 1) * (grid_size[1] - 2) + (i[1] - 1)
            else:
                return i[0] * grid_size[1] + i[1]


    def edgeIndex(Nx, Ny, i):
        #Going around edges clockwise.
        if type(i) is int:

            if 0 <= i <= Ny - 1:
                return 0, i
            elif Ny <= i <= Ny + Nx - 2:
                return i - Ny + 1, Ny - 1
            elif Ny + Nx - 1 <= i <= Ny*2 + Nx - 3:
                return Nx - 1, Ny*2 - i + Nx - 3
            elif Ny*2 + Nx - 2 <= Ny*2 + Nx*2 - 4:
                return Nx*2 - i + Ny*2 - 4, 0
        elif type(i) is tuple and len(i) == 2:
            if i[0] == 0: return i[1]
            elif i[1] == Ny - 1: return Ny - 1 + i[0]
            elif i[0] == Nx - 1: return Ny + Nx - 2 + Ny - 1 - i[1]
            elif i[1] == 0: return Ny*2 + Nx*2 - 4 - i[0]


    def isOnEdge(Nx, Ny, i):
        if i[0] == 0: return 1 # Eslov
        elif i[1] == Ny - 1: return 2 # Sodra Sandby
        elif i[0] == Nx - 1: return 3 # Malmö
        elif i[1] == 0: return 4 #Lomma
        else: return 0


class Complex:

    def __init__(self, ppUL, tempVec, w, maxIter):
        self.comm = MPI.COMM_WORLD
        self.rank = self.comm.Get_rank()


        self.p = ppUL
        self.pp = self.p//2
        self.h = 1 / (self.p - 1)
        self.tH = tempVec[0] #Temp High
        self.tWin = tempVec[1] #Temp Normal
        self.tW = tempVec[2] #Temp Wall

        self.w = w
        self.maxIter = maxIter

    def calc(self):
        if self.rank == 0:
            boundary1 = [self.tH] + [self.tW] * (self.p - 1) + [0] * (self.p - 2) + [self.tWin] + [self.tW] * (self.p - 2) + [self.tH] * (self.p - 1)
            boundary1_type = [True] * self.p + [False] * (self.p - 2) + [True] * (2 * self.p - 2)
           
            boundary2 = [self.tH] * self.p + [20] * (self.p - 2) + [self.tW] * (self.p - 1) + [self.tWin] * self.p + [20] * (self.p - 2) + [self.tW] * (self.p - 1)
            boundary2_type = [True] * (6*self.p - 6)

            boundary4 = [self.tW] + [self.tW] * (self.pp - 2) + [self.tW] * self.pp + [self.tH] * (self.pp - 1) + [0] * (self.pp - 2)
            boundary4_type = [True] * (3 * self.pp - 2) + [False] * (self.pp - 2)
        



            room1 = Comp((self.p, self.p), self.h, boundary1, boundary1_type)
            room2 = Comp((self.p * 2 - 1, self.p), self.h, boundary2, boundary2_type)
            room4 = Comp((self.pp, self.pp), self.h, boundary4, boundary4_type)
            

            sol1 = np.zeros([self.p, self.p])
            sol2 = np.zeros([self.p * 2 - 1, self.p])
            sol4 = np.zeros([self.pp, self.pp])
            

            for i in range(self.maxIter):
                sol2_new = room2.calc()
                bound_to_1 = (sol2_new[-1-(self.p-2):-1, 1] - sol2_new[-1-(self.p-2):-1, 0]) / self.h
                bound_to_3 = -(sol2_new[1:self.p-1, -1][::-1] - sol2_new[1:self.p-1, -2][::-1]) / self.h
                bound_to_4 = -(sol2_new[self.p+1:self.p*2-2-(self.pp), -1][::-1] - sol2_new[self.p+1:self.p*2-2-(self.pp), -2][::-1]) / self.h
                #print(len(bound_to_4))
                

                self.comm.send(bound_to_3, 1)
                room1.boundary[self.p:2*self.p-2] = bound_to_1
                room4.boundary[self.pp*3-2:4*self.pp-4] = bound_to_4
                #print(len(room4.boundary[self.p*3-2:4*self.p-4-(self.pp)]))
                sol1 = self.w * room1.calc() + (1 - self.w) * sol1
                sol2 = self.w * sol2_new + (1 - self.w) * sol2
                sol3 = self.comm.recv(source=1)
                sol4 = self.w * room4.calc() + (1 - self.w) * sol4

                bound_from_1 = sol1[1:-1, -1][::-1]
                bound_from_3 = sol3[1:-1, 0]
                bound_from_4 = sol4[1:-1, 0]

                room2.boundary[-(2*self.p-3):-(self.p-1)] = bound_from_1
                room2.boundary[self.p:2*self.p-2] = bound_from_3
                room2.boundary[self.p*2:3*self.p-3-(self.pp)] = bound_from_4 ##?????
                

            return sol1, sol2, sol3, sol4

        if self.rank == 1:
            boundary3 = [self.tH] + [self.tW] * (self.p - 2) + [self.tH] * self.p + [self.tW] * (self.p - 1) + [0] * (self.p - 2)
            boundary3_type = [True] * (3 * self.p - 2) + [False] * (self.p - 2)

            room3 = Comp((self.p, self.p), self.h, boundary3, boundary3_type)


            sol3 = np.zeros([self.p, self.p])
            sol4 = np.zeros([self.p, self.p])

            for i in range(self.maxIter):
                bound_from_2 = self.comm.recv(source=0)
                room3.boundary[-(self.p - 2):] = bound_from_2

                sol3 = self.w * room3.calc() + (1 - self.w) * sol3

                self.comm.send(sol3, 0)

    def plot(self):
        
        sol = self.calc()
        if self.rank != 0:
            return

        apartment_area = np.zeros((2*(self.p-1)+1, 3*(self.p-1)+1))
        apartment_area[self.p-1:, :self.p] = np.array(sol[0])
        apartment_area[:, self.p-1:2*self.p-1] = np.array(sol[1])
        apartment_area[:self.p, 2*self.p-2:] = np.array(sol[2])
        
        
        apartment_area[self.p:self.p-1+self.pp+1, 1+2*self.p-2:1+2*self.p-2+self.pp] = np.array(sol[3])
        #print(sol[3].shape)
        #print(apartment_area[self.p:self.p-1+self.pp+1, 2*self.p-2:2*self.p-2+self.pp].shape)
        plt.imshow(apartment_area, interpolation=None, cmap="inferno")
        plt.colorbar()
        plt.show()


#a = Complex(21, [40,5,15], 0.8, 20)
a = Complex(41, [40,5,15], 0.8, 20)
a.plot()
