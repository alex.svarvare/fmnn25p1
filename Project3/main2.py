from oliver import Omega, border
from mpi4py import MPI
import numpy as np
from scipy.ndimage import shift
import matplotlib.pyplot as plt


def main():
    omega = 0.8
    d1 = d2 = d3 = False
   
    comm = MPI.Comm.Clone(MPI.COMM_WORLD)

    size = comm.Get_size()
    
    rank = comm.Get_rank()
    N = 100
    print("initializing omegas")
    #O2 = Omega(1, 1, N, N, 1.0/N)
    if rank<5: 
        print("initializing omegas done, now borders")
        T1 = border(40.0, N)
        shared = border(20.0, N)
        L1 = border(15.0, N)
        R_shared = border(10.0, N)
        B2 = border(5.0, N)
        L_shared = R_shared
        R2 = border(15.0, N)
        T3 = border(15.0, N)
        B3 = border(15.0, N)
        L3 = border(40.0, N)
        T4 = border(15.0, N)
        B4 = border(15.0, N)
        R4 = border(40.0, N)
    
  
    print("initializing borders")
    #while d1 or d2 or d3:
   # if size < 3:
     #       print("Add more processes")
    #        raise Exception("NP must be >3.")
    # Stora
    if rank == 0:
            O2 = Omega(1,1,N,N,1.0/N)
            O1 = Omega(1, 1, N, N, 1.0/N)
            
            for i in range(20):
                print("i stora gång nr ", i )
                if i == 0:
                    data1 = comm.irecv(1)  
                    data2 = comm.irecv(2)
                else:
                     data1 = comm.recv(1)
                     data2 = comm.recv(2)
                O1.addBoarders(T1, shared, L1, R_shared)
                O1.createSparse()
                solv1 = O1.Solve_sys("N","R")
                res = O1.dirichlet(1, N, shared, "B")
                shared.setTemp_shared(res[0:N], 0, N)
                O2.addBoarders(shared, B2, L_shared, R2)
                O2.createSparse()
                solv2 = O1.Solve_sys("N","L")
                res2 = O1.dirichlet(1, N, shared, "T")
                res4 = O1.dirichlet(1, N, R_shared, "R")
                res5 = O1.dirichlet(1, N, L_shared, "L")
                R_shared.setTemp_shared(res4[0:N], 0, N)
                L_shared.setTemp_shared(res5[0:N], 0, N)
                shared.setTemp_shared(res2[0:N], 0, N)
                comm.send(L_shared, dest =1 )
                comm.send (R_shared, dest = 2)
            comm.send((solv1,solv2),dest =3)    
      #  d1=False


    # Litet vänster
    if rank == 1:
            O3 = Omega(1, 1, N, N, 1.0/N)
    
            for i in range(20):
                print("i lilla gång nr ", i)            
                data = comm.recv(source = 0)
                O3.addBoarders(T3, B3, L3, data)
                O3.createSparse()
                solv3 = O3.Solve_sys("N","R")
                res = O3.dirichlet(1, N, L_shared, "R")
                L_shared.setTemp_shared(res[0:N], 0, N)
                comm.send(L_shared, dest = 0)
            comm.send(solv3,dest =3)
    # Liten höger
    if rank == 2:
            O4 = Omega(1, 1, N, N, 1.0/N)
    
            for i in range(20):
                
                print("i lilla höger gång nummer",i)
                data = comm.recv(source = 0)
                O4.addBoarders(T4, B4, R_shared, R4)
                O4.createSparse()
                solv4 = O4.Solve_sys("N","L")
                res = O4.dirichlet(1, N, data, "L")
                R_shared.setTemp_shared(res[0:N], 0, N)
                comm.send(R_shared, dest = 0)
            comm.send(solv4,dest =3)

    #solv1 = 
    if rank == 3:
        solv4 = comm.recv(source = 2)
        solv3 = comm.recv(source = 1)
        solv1,solv2 = comm.recv(source = 0)
        kk1 = np.concatenate((solv4, solv4*0,))
        kk2 = np.concatenate((solv1, solv2))
        kk3 = np.concatenate((solv3*0, solv3))

        kkk4 = np.concatenate((kk3, kk2), axis=1)
        kkk5 = np.concatenate((kkk4, kk1), axis=1)

        plt.imshow(kkk5, cmap='hot', interpolation='none')
        plt.title("Heatmap of the rooms")
        plt.show() 
        comm.Abort(0)


if __name__ == "__main__":
    main()
