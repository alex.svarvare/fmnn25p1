import numpy as np


def __init__(self, dim, delta, wall_types):    ## dim -> 2x1 vektor, pga rektangulart rum
	self.dim = dim
	self.delta = delta
	self.wall_types = wall_types
	

def simulate(self):
	grid_points = (self.dim[0] - 2) * (self.dim[1] - 2)    ## vaggarna behandlas separat
	a = np.zeros([grid_points], [grid_points])
	b = np.zeros(grid_points)
	
	
