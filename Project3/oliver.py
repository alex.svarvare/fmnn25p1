from scipy.sparse import csr_matrix, csc_matrix
import numpy as np
from scipy.sparse import diags
from scipy.sparse import coo_matrix, vstack
from scipy . sparse . linalg import spsolve, cg
from scipy.ndimage import shift
import matplotlib.pyplot as plt


class border():

    # temperature coefficient
    # shared_vec with length N. equal 1 if shared, otherwise 0
    # the temperature in vec_boarder[i] if not shared is temperature

    def __init__(self, temperature, N, shared_vec=None, initialTemp=0, shared=False) -> None:
        self.temperature = temperature
        self.shared = shared

        self.shared_vec = shared_vec
        self.N = N
        N = N
        self.temperature_array = np.zeros(N+1)

        for i in range(N+1):
            if shared:
                if self.shared_vec[i] == 1:
                    self.temperature_array[i] = initialTemp
                else:
                    self.temperature_array[i] = initialTemp
            else:
                self.temperature_array[i] = temperature

    def setTemp(self, temperature_vec):
        for i in range(self.N):
            if self.shared_vec[i] == 1:
                self.temperature_array[i] = temperature_vec[i]

    def setTemp_shared(self, temp_vec, start, stop):
        # print("_*****")
        # print(temp_vec)
        # print(  self.temperature_array)
        w = 0.8
        self.temperature_array[start:stop] = temp_vec * \
            w + (1-w)*self.temperature_array[start:stop]


class Omega():

    def __init__(self, lengthX, lengthY, Nx, Ny, h) -> None:
        self.h = h

        self.Nx = Nx  # ska vara lengthX*h
        self.Ny = Ny  # ska vara LengthY*h

        self.y = np.zeros((self.Nx-1)*(self.Ny-1))

    def addBoarders(self, border_top, border_bottom, border_left, border_right):

        self.border_top = border_top
        self.border_bottom = border_bottom
        self.border_left = border_left
        self.border_right = border_right

    def pos_is_in(self, N, Nx, Ny):
        i, j = self.getCord(N)
        if j == 0:
            return False

        if j == Ny:
            return False

        if i == 0:
            return False
        if i == Nx:
            return False
        return True

    def Neumann(self,start,stop,wall:border,WallSide):
        N=wall.N
        h=self.h # ??
        width =  (self.Nx+1)*(self.Nx+1)
        zero=[0.0]*(self.Nx-1)
        
        res=np.zeros(stop+1)
        
        if WallSide=="L":
            param=1
            for i in range(start,stop):
                    res[i]= - (1/h) * (  -3* self.grid[i,param]+self.grid[i+1,param]+self.grid[i-1,param]+self.grid[i,param+1]     )
            res[start-1]=res[start]
            res[stop]=res[stop-1]

        if WallSide == "R":
            param = self.Nx-1
            for i in range(start, stop):
                res[i] = - (1/h) * (-3 * self.grid[i, param]+self.grid[i+1,
                                                                       param]+self.grid[i-1, param]+self.grid[i, param-1])
            res[start-1] = res[start]
            res[stop] = res[stop-1]

        if WallSide == "T":
            param = 1
            # print("______________________")
            # print(self.grid)
            for i in range(start, stop):
                # print("_____T_____")
                # print(self.grid[param,i],self.grid[param,i+1],self.grid[param,i-1],self.grid[param-1,i])



                    res[i]= -(1/h) * (  -3* self.grid[param,i]+self.grid[param,i+1]+self.grid[param,i-1]+self.grid[param+1,i]     )
            res[start-1]=(res[start]+self.grid[param,0])/2
            res[stop+1]=(res[stop-1]+self.grid[param,stop-1])/2
            print(res)
        if WallSide=="B":
            param=self.Ny-1
            #print("________B________________")
            #print(self.grid)
            for i in range(start,stop):
                   # print("Nedan")
             #       print(self.grid[param,i],self.grid[param,i+1],self.grid[param,i-1],self.grid[param-1,i])
                res[i] = -(1/h)*(-3 * self.grid[param, i]+self.grid[param,
                                                                    i+1]+self.grid[param, i-1]+self.grid[param-1, i])
            res[start-1] = (res[start]+self.grid[param][0])/2
            res[stop] = (res[stop-1]+self.grid[param][stop-1])/2

        return res


    def dirichlet(self,start,stop,wall:border,WallSide):
        N=wall.N
         # ??
        width =  (self.Nx+1)*(self.Nx+1)
        zero=[0.0]*(self.Nx-1)
        
        res=np.zeros(stop+1)
        if WallSide=="L":
            param=1
            #print("__________LEFT___")
            #print(self.grid)
            for i in range(start,stop):
             #       print(self.grid[i,param],self.grid[i+1,param],self.grid[i-1,param],self.grid[i,param+1]  )
                    res[i]=-1*(  -4* self.grid[i,param]+self.grid[i+1,param]+self.grid[i-1,param]+self.grid[i,param+1]     )
            res[start-1]=(res[start]+self.grid[0][param])/2
            res[stop]=(res[stop-1]+self.grid[stop-1][param])/2
            #res[start-1]=res[start]
            #res[stop]=(res[stop-1])
            #print("left")
            #print(res)
        if WallSide == "R":
            #print("__________RIGHT_____________-")
            #print(self.grid)
            param=self.Nx-1
            for i in range(start,stop):
              #      print( self.grid[i,param],self.grid[i+1,param],self.grid[i-1,param],self.grid[i,param-1])
                    res[i]=-1*(  -4* self.grid[i,param]+self.grid[i+1,param]+self.grid[i-1,param]+self.grid[i,param-1]     )
            res[start-1]=(res[start]+self.grid[0][param])/2
            res[stop]=(res[stop-1]+self.grid[stop-1][param])/2
            #res[start-1]=res[start]
            #res[stop]=res[stop-1]
            #print("right")
            #print(res)
        if WallSide=="T":
            param=1
           # print("_______________________--")
           # print(self.grid)
            for i in range(start,stop):
             #       print(self.grid[param,i],self.grid[param,i+1],self.grid[param,i-1],self.grid[param+1,i])
                    res[i]= -1 * (  -4* self.grid[param,i]+self.grid[param,i+1]+self.grid[param,i-1]+self.grid[param+1,i]     )
            #res[start-1]=res[start]
            #res[stop]=res[stop-1]
            #res[start-1]=(res[start]+self.grid[param,0])/2
            #res[stop]=(res[stop-1]+self.grid[param,stop-1])/2
            #print(res)
        if WallSide=="B":
            param=self.Ny-1
            #parm=self.Ny-1
            #print("___BOTTOM_____________________")
            #print(self.grid)

            for i in range(start,stop):
                    #print("----",self.grid[param,i],self.grid[param,i+1],self.grid[param,i-1],self.grid[param-1,i]    )
                    res[i]=-1.0 * (  -4.0* self.grid[param,i]+self.grid[param,i+1]+self.grid[param,i-1]+self.grid[param-1,i]     )
                    #print(res[i])
            res[start-1]=res[start]
            res[stop]=res[stop-1]
            res[start-1]=(res[start]+self.grid[param][0])/2
            res[stop]=(res[stop-1]+self.grid[param][stop-1])/2
            #print(res)

     
        return res
    
    def lol(self,start,stop,wall:border,WallSide):
        N=wall.N
          
        res=np.zeros(stop+1)
        if WallSide=="L":
            param=1
           
            for i in range(start,stop):
            
                res[i]=(self.grid[i,param+1]+self.grid[i,param]+self.grid[i+1,param]+self.grid[i-1,param] )/4
            res[(start-1)]=res[start]
            res[(stop)]=res[(stop-1)] 
            res[0]=self.grid[0][0]
            res[stop]=self.grid[0][stop]
            #return (self.grid[1]-self.grid[0]) / (1/N)
            #print("LEFT")
            #print(res)
          
        if WallSide == "R":
   
           
            param=self.Nx-1
            for i in range(start,stop):
                
                res[i]=(self.grid[i,param]+self.grid[i,param-1] +self.grid[i+1,param]+ self.grid[i-1,param] )/4
            res[(start-1)]=res[start]
            res[(stop)]=self.grid[0][param]
            res[start]=self.grid[start][param]
            #return (self.grid[self.Nx-1]-self.grid[self.Nx-2])/(1/N)
       
        if WallSide=="T":
            param=1
         
            for i in range(start,stop):
             
                    res[i]=  (self.grid[param][i]+self.grid[param-1][i])/2
            res[start-1]=res[start]
            res[stop]=res[stop-1]

        if WallSide=="B":
            param=self.Ny-1
       
            for i in range(start,stop):
                #print("----",self.grid[param,i],self.grid[param,i+1],self.grid[param,i-1],self.grid[param-1,i]    )
                    res[i]=(self.grid[param,i]+self.grid[param-1,i])/2
                    
            res[start-1]=res[start]
            res[stop]=res[stop-1]
      
          

     
        return res
    

    
    def isBoarder(self,direction,index):
        row,col = self.getCord(index)
        #print(index,row,col,row==1)
        if direction=="L":
            return col==1
        elif direction=="R":
            return col==self.Nx-1
        elif direction=="T":
            return row==self.Ny-1
        elif direction=="B":
            return row==1

            


        return True


    def createSparse2(self,BondWall):

        self.y = np.zeros((self.Nx-1)*(self.Ny-1))
        Nx = self.Nx
        Ny = self.Ny
        width = (Nx+1)*(Nx+1)-2
        h=1/(Nx)
        


        zero = [0.0]*(Nx-1)
        diag = [1.0]+zero+[1.0, -4.0, 1.0]+zero+[1.0]   ## -4 -> -3  

        h1,h2,h3,h4=1,1,1,1
        if BondWall=="L":
            h4=h   
        elif BondWall=="R":
            h2=h
        elif BondWall=="T":
            h1=h
        elif BondWall=="B":
            h3=h


        diag2 = [1.0*h3]+zero+[1.0*h4, -3.0, 1.0*h2]+zero+[1.0*h1]
        r = np.zeros(((Nx-1)*(Ny-1), width))
        last_index = 0
        for i in range((Nx-1)*(Ny-1)):
            for j in range(last_index, width-len(diag)+1):
                fut = Nx+2

                if self.pos_is_in(fut+j, Nx, Ny):
                    if self.isBoarder(BondWall,fut+j):
                        r[i][j:(j+len(diag))]=diag2 
                        last_index = j+1
                        break
                        
                        
                    r[i][j:(j+len(diag))] = diag
                    last_index = j+1
                    break
                
                   
            
        #print(r)
        self.room = csr_matrix(r)

        tmp = self.getKnownV()

        for i in range(0, len(tmp)):
            position, value = tmp[i]
            self.y = np.append(self.y, value)
            B = coo_matrix(([1.0], ([0], [position-1])), shape=(1, width))
            self.room = vstack([self.room, B])



    '''
    def Solve_sys2(self):
        k = np.array(self.room.toarray())

        tmp = csr_matrix(np.array(self.y))

        X = spsolve(csr_matrix(self.room), ((self.y)))

        # sol= np.linalg.solve(self.room.toarray(),self.y  )

        # print(X)
        solultion_a = X
        temp = self.border_top.temperature_array[self.border_top.N-1]
        solultion_a = np.append(solultion_a, temp)
        solultion_a = np.insert(
            solultion_a, 0, self.border_bottom.temperature_array[0])

        solultion_a = np.flipud(solultion_a)
        solultion_a = np.reshape(solultion_a, (self.Nx+1, self.Ny+1))

        self.grid = np.flip(solultion_a, 1)

        return self.grid
    '''

    def createSparse(self):

        self.y = np.zeros((self.Nx-1)*(self.Ny-1))
        Nx = self.Nx
        Ny = self.Ny
        width = (Nx+1)*(Nx+1)-2

        zero = [0.0]*(Nx-1)
        diag = [1.0]+zero+[1.0, -4.0, 1.0]+zero+[1.0]   ## -4 -> -3     

        r = np.zeros(((Nx-1)*(Ny-1), width))
        last_index = 0
        for i in range((Nx-1)*(Ny-1)):
            for j in range(last_index, width-len(diag)+1):
                fut = Nx+2

                if self.pos_is_in(fut+j, Nx, Ny):

                    r[i][j:(j+len(diag))] = diag
                    last_index = j+1
                    break

        self.room = csr_matrix(r)

        tmp = self.getKnownV()

        for i in range(0, len(tmp)):
            position, value = tmp[i]
            self.y = np.append(self.y, value)
            B = coo_matrix(([1.0], ([0], [position-1])), shape=(1, width))
            self.room = vstack([self.room, B])

    def Solve_sys(self,type,side=None):
        
        self.room=None
        if type=="N":
            self.createSparse2(side)
        else:
            self.createSparse()
        

        k = np.array(self.room.toarray())

        tmp = csr_matrix(np.array(self.y))

        X = spsolve(csr_matrix(self.room), ((self.y)))

        # sol= np.linalg.solve(self.room.toarray(),self.y  )

        # print(X)
        solultion_a = X
        temp = self.border_top.temperature_array[self.border_top.N-1]
        solultion_a = np.append(solultion_a, temp)
        solultion_a = np.insert(
            solultion_a, 0, self.border_bottom.temperature_array[0])

        solultion_a = np.flipud(solultion_a)
        solultion_a = np.reshape(solultion_a, (self.Nx+1, self.Ny+1))

        self.grid = np.flip(solultion_a, 1)

        return self.grid

    # return an array containing the known Vs as touple  (position number, value)

    def getKnownV(self):
        tmp = []
        top_t = self.border_bottom.temperature_array
        for i in range(1, len(top_t)):
            res = self.getNbr(0, i)
            tmp.append((res, top_t[i]))
        top_t = self.border_left.temperature_array
        for i in range(1, len(top_t)-1):
            res = self.getNbr(i, 0)
            tmp.append((res, top_t[i]))
        top_t = self.border_right.temperature_array
        for i in range(1, len(top_t)-1):
            res = self.getNbr(i, self.Ny)
            tmp.append((res, top_t[i]))
        top_t = self.border_top.temperature_array
        for i in range(0, len(top_t)-1):
            res = self.getNbr(self.Nx, i)
            tmp.append((res, top_t[i]))
        #print()
        #print(tmp)
        return tmp

    def getNbr(self, i, j):
        return i*(self.Nx+1)+j

    def getCord(self, N):
        row = N // (1+self.Nx)
        col = N % (1+self.Nx)
        return (row, col)



#test(100)
    '''
def test(N):


    Omega2a=Omega(1,1,N,N,1.0/N)
    Omega2b=Omega(1,1,N,N,1.0/N)
    Omega1=Omega(1,1,N,N,1.0/N)
    Omega3=Omega(1,1,N,N,1.0/N)




    Top2a,left2a=border(40.0,N),border(15.0,N)
    Right2a_Left3=border(15,N)
    Bottom2a_Top2b=border(15.0,N)

    Right2b,Bottom2b=border(15.0,N),border(5.0,N)
    Left2b_Right1=border(15.0,N)

    Right3,Top3,Bottom3=border(40.0,N),border(15.0,N),border(15.0,N)


    Top1,Bottom1,Left1=border(15.0,N),border(15.0,N),border(40.0,N)

   

    for i in range(10):

        Omega2a.addBoarders(Top2a,Bottom2a_Top2b,left2a,Right2a_Left3)
        solv_2a=Omega2a.Solve_sys("D")
      
        res=Omega2a.lol(1,N,Bottom2a_Top2b,"B")
        res=Omega2a.grid[-2, 1:-1] 
        Bottom2a_Top2b.setTemp_shared(res,1,N)
        

        Omega2b.addBoarders(Bottom2a_Top2b,Bottom2b,Left2b_Right1,Right2b)
        solv_2b=Omega2b.Solve_sys("D")

        
        res2=Omega2b.lol(1,N,Bottom2a_Top2b,"T")
        res4=Omega2a.lol(1,N,Right2a_Left3,"R")
        res5=Omega2b.lol(1,N,Left2b_Right1,"L")
        #print(res2,res5,res4)
    
        res2=Omega2b.grid[1, 1:-1]
        res4=Omega2a.grid[1:-1, 1][::-1]
        res5=Omega2b.grid[1:-1, -2][::-1]


        #  Left_wall=c[1:-1, -2][::-1]
        # Right_wall= [1:-1, 1][::-1]
        # Top wall = c[1, 1:-1]
        # bot wall c[-2, 1:-1] 

        Right2a_Left3.setTemp_shared(res4,1,N)
        Left2b_Right1.setTemp_shared(res5,1,N)
        Bottom2a_Top2b.setTemp_shared(res2,1,N)
      ##      för vänster ner

        Omega3.addBoarders(Top3,Bottom3,Right2a_Left3,Right3)

        
        solv_3=Omega3.Solve_sys("N","L")
        #print(Right2a_Left3.temperature_array)
        #print(solv_3)
        res=Omega3.lol(1,N,Right2a_Left3,"L")
        res=Omega3.grid[1:-1, -2][::-1]
        Right2a_Left3.setTemp_shared(res,1,N)



        Omega1.addBoarders(Top1,Bottom1,Left1,Left2b_Right1)
        solv_1=Omega1.Solve_sys("N","R")
        #res=O4.dirichlet(1,N,R_shared,"L")
        res=Omega1.lol(1,N,Left2b_Right1,"R")
        res=Omega1.grid[1:-1, 1][::-1]
        Left2b_Right1.setTemp_shared(res,1,N)


    Omega2a.addBoarders(Top2a,Bottom2a_Top2b,left2a,Right2a_Left3)
    Omega2b.addBoarders(Bottom2a_Top2b,Bottom2b,Left2b_Right1,Right2b)
    solv_2b=Omega2b.Solve_sys("D")
    solv_2a=Omega2a.Solve_sys("D")
    res4=Omega2a.grid[1:-1, 1][::-1]
    res5=Omega2b.grid[1:-1, -2][::-1]

    Right2a_Left3.setTemp_shared(res4,1,N)
    Left2b_Right1.setTemp_shared(res5,1,N)

    Omega2a.addBoarders(Top2a,Bottom2a_Top2b,left2a,Right2a_Left3)
    Omega2b.addBoarders(Bottom2a_Top2b,Bottom2b,Left2b_Right1,Right2b)
    solv_2b=Omega2b.Solve_sys("D")
    solv_2a=Omega2a.Solve_sys("D")

    Omega1.addBoarders(Top1,Bottom1,Left1,Left2b_Right1)
    solv_1=Omega1.Solve_sys("D","R")
    Omega3.addBoarders(Top3,Bottom3,Right2a_Left3,Right3)
    Omega2b.addBoarders(Bottom2a_Top2b,Bottom2b,Left2b_Right1,Right2b)
    solv_2b=Omega2b.Solve_sys("D")
        
    solv_3=Omega3.Solve_sys("D","L")

    print("s1",solv_1)
    print("s2",solv_2a)
    print("s2b",solv_2b)
    print("s3",solv_3)

    kk1=np.concatenate(( solv_3,solv_3*0,))
    kk2=np.concatenate((solv_2a, solv_2b))
    kk3=np.concatenate(( solv_1*0,solv_1))

    kkk4 = np.concatenate((kk3, kk2), axis=1)
    kkk5 = np.concatenate((kkk4, kk1), axis=1)

    plt.imshow(kkk5, cmap='hot', interpolation='none')
    plt.title("FRÅN TEST")
    plt.show()


#test(6)







def test2(N):
    p = Omega(3, 3, N, N, 3)
    T = border(15.0, N)
    B = border(15.0, N)
    L = border(50.0, N)
    R = border(20.0, N)
    R.temperature_array[1:4]=[-10,-20,-30]
    print(R.temperature_array)
    p.addBoarders(T, B, L, R)
    #p.createSparse2("L")

    print(R.temperature_array)
    solultion_a = p.Solve_sys("N","L")
    t=p.lol(0,N,R,"R")
    print(t)
    R.setTemp_shared(t,0,N+1)
    print(R.temperature_array)

    print()
    print(solultion_a)
    print()

    #plt.imshow(solultion_a, cmap='hot', interpolation='none')
    #plt.show()


k=[1,2,3,4,5]
c=np.array( [ [1,2,3,4,6],[102,11,22,33,44],[77,4,5,6,7],[1,8,9,10,11 ],[-1,0,-1,0,-1]] )
print(c)
t=c[1:-1, -1][::-1]


#  Left_wall=c[1:-1, -2][::-1]
# Right_wall= [1:-1, 1][::-1]
# Top wall = c[1, 1:-1]
# bot wall c[-2, 1:-1] 

v=c[-2, 1:-1] 
#print(t)
#print(k)
print(v)

test(80)

#k[0:3]=[4,9,0]
#print(k)
#test2(6)

def sol3():
    lol = np.array([
        [4.0, -1.0, -1, 0],
        [-1, 4, 0, -1],
        [0, -1.0, -1, 4.0],
        [-1, 0, 4.0, -1]
    ])

    lolp = np.array([15.0, 30.0, 35.0, 20.0])

    print("idiotiskt",np.linalg.solve(lol, lolp))
    '''
