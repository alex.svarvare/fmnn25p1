import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation

max_iteration = 1000
t_n = 15
t_h = 40
t_w = 5
t_init = 0

alpha = 2 # deltaT=alpha*nablaT

# u means temp on point, du means heat flow, ddu means change of heatflow. If ddu = 0 for many iterations. Eqvilibrium has been met.

h = 1/50
dt = 1/2*h**2/4/alpha
gamma = alpha*dt/h**2

#First room definitions
wallsVal = (t_n, t_init, t_n, t_h)
boundCond = (False, True, False, False)


class compartement:

    def __init__(self, floorPlan, h, wallsVals, boundCond): 
        """
        :floorPlan says relative room structure. Ex Omega1 = (1,1)
        :h is dx grid spacing
        :wallsVal are the starting values for walls.
        :BoundCond are the condition on walls. North, going clockwise. To see if west wall is Neuman is "if boundCond[1]" Should be true if west wall is Neumann
        """

        self.floorPlan = floorPlan
        self.hh = int(1/h)
       
        self.floorPlanGrid = (self.floorPlan[0]*self.hh, self.floorPlan[1]*self.hh)


        self.u = np.zeros((floorPlan[0]*self.hh, floorPlan[1]*self.hh)) 

        self.u[0,:] = wallsVals[0] #top wall
        self.u[:,self.floorPlanGrid[1]-1] = wallsVals[1] #right wall
        self.u[self.floorPlanGrid[0]-1,:] = wallsVals[2] #bottom wall
        self.u[:, 0] = wallsVals[3] #left wall
    

        #Small ambiguity here. Depending on how they are assigned. One wall may get one more static temp since vertecies overlap.

    def solve(self):
        print(self.u.shape)
        u_temp = np.zeros(self.u.shape)
        u_temp = self.u # Propagate last sol.
        
        for i in range(1,self.floorPlanGrid[0]-2):
            for j in range(1,self.floorPlanGrid[1]-2):
                u_temp[i,j] = gamma * (self.u[i+1][j] - 4*self.u[i][j] + self.u[i-1][j] + self.u[i][j+1] + self.u[i][j-1]) + self.u[i][j]
        
        self.u = u_temp

def plotShit(u_k, k):
    plt.clf()

    plt.title(f"Temp vid iter = {k}")

    plt.xlabel("x")
    plt.ylabel("y")

    plt.pcolormesh(u_k, cmap="inferno", vmin=t_w, vmax=t_h)
    plt.colorbar()

    return plt

r1 = compartement((1,1), h, wallsVal,  boundCond)


for i in range(max_iteration):
    r1.solve()

plotShit(r1.u,max_iteration)



def animate(u, k):
  plotShit(u[k], k)

#plotShit(u[max_iteration-1], max_iteration-1)
plt.show()
#anim = animation.FuncAnimation(plt.figure(), animate, interval=0.00000001, frames=max_iteration-2, repeat=False)
#plt.show()