from oliver import *
from mpi4py import MPI


def main():
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    N = 100

    O1 = Omega(1, 1, N, N, 1.0/N)
    O2 = Omega(1, 1, N, N, 1.0/N)
    O3 = Omega(1, 1, N, N, 1.0/N)
    O4 = Omega(1, 1, N, N, 1.0/N)
    T1 = border(40.0, N)
    shared = border(20.0, N)
    L1 = border(15.0, N)
    R_shared = border(10.0, N)
    B2 = border(5.0, N)
    L_shared = border(10.0, N)
    R2 = border(15.0, N)
    T3 = border(15.0, N)
    B3 = border(15.0, N)
    L3 = border(40.0, N)
    T4 = border(15.0, N)
    B4 = border(15.0, N)
    R4 = border(40.0, N)

    if size < 4:
        print("Add more processes")
        raise Exception("NP must be >3.")

    # Stora
    if rank == 0:
        for i in range(20):
            O1.addBoarders(T1, shared, L1, R_shared)
            O1.createSparse()
            solv = O1.Solve_sys()
            res = O1.dirichlet(1, N, shared, "B")
            shared.setTemp_shared(res[0:N], 0, N)
            O2.addBoarders(shared, B2, L_shared, R2)
            O2.createSparse()
            solv2 = O2.Solve_sys()
            res2 = O2.dirichlet(1, N, shared, "T")
            res4 = O1.dirichlet(1, N, R_shared, "R")
            res5 = O2.dirichlet(1, N, L_shared, "L")
            R_shared.setTemp_shared(res4[0:N], 0, N)
            L_shared.setTemp_shared(res5[0:N], 0, N)
            shared.setTemp_shared(res2[0:N], 0, N)

    # Litet vänster
    if rank == 1:
        for i in range(20):

            O3.addBoarders(T3, B3, L3, L_shared)
            O3.createSparse()
            solv3 = O3.Solve_sys()
            res = O3.dirichlet(1, N, L_shared, "R")
            L_shared.setTemp_shared(res[0:N], 0, N)

    # Liten höger
    if rank == 2:
        for i in range(20):

            O4.addBoarders(T4, B4, R_shared, R4)
            O4.createSparse()
            solv4 = O4.Solve_sys()
            res = O4.dirichlet(1, N, R_shared, "L")
            R_shared.setTemp_shared(res[0:N], 0, N)

    solv1 = O1.Solve_sys()

    kk1 = np.concatenate((solv4, solv4*0,))
    kk2 = np.concatenate((solv1, solv2))
    kk3 = np.concatenate((solv3*0, solv3))

    kkk4 = np.concatenate((kk3, kk2), axis=1)
    kkk5 = np.concatenate((kkk4, kk1), axis=1)

    plt.imshow(kkk5, cmap='hot', interpolation='none')
    plt.show()


if __name__ == "__main__":
    main()
