import numpy as np
from inspect import signature
import matplotlib.pyplot as plt

h = 0.001
tol = 0.0000001
myu = 0.001

# tol = 2*np.finfo(np.float64).eps

# Plot of Rosenbrock's banana function: f(x,y)=(1-x)^2+100(y-x^2)^2

rosenbrockfunction = lambda x, y: (1 - x) ** 2 + 100 * (y - x**2) ** 2

n = 100  # number of discretization points along the x-axis
m = 100  # number of discretization points along the x-axis
a = -0.5
b = 2.0  # extreme points in the x-axis
c = -1.5
d = 4.0  # extreme points in the y-axis

X, Y = np.meshgrid(np.linspace(a, b, n), np.linspace(c, d, m))

Z = rosenbrockfunction(X, Y)

ppp = plt.contour(X, Y, Z, np.logspace(-0.5, 3.5, 8, base=10), cmap="gray")
plt.clabel(ppp, inline=True, fontsize=8)

plt.title("Rosenbrock Function:  f(x,y)=(1-x)^2+100(y-x^2)^2$")
plt.xlabel("x")
plt.ylabel("y")
plt.rc("text", usetex=True)
plt.rc("font", family="serif")


def rosenbrock(x_0, x_1):
    return 100 * (x_1 - x_0**2) ** 2 + (1 - x_0) ** 2


def grad(X, f):
    grad = np.zeros(len(X))
    grad[0] = (f(X[0] + h, X[1]) - f((X[0] - h), X[1])) / (2 * h)
    grad[1] = (f(X[0], X[1] + h) - f(X[0], X[1] - h)) / (2 * h)

    return grad


def Jacob(X, f_list):
    Jacob = np.zeros(len(f_list), len(X))
    for i in range(len(f_list)):
        Jacob[i] = grad(X, f_list[i])
    return Jacob


def Hess(X, f):
    Hess = np.zeros((len(X), len(X)))
    Hess[0][0] = (f(X[0] + h, X[1]) - 2 * f(X[0], X[1]) + f(X[0] - h, X[1])) / (h**2)
    Hess[1][1] = (f(X[0], X[1] + h) - 2 * f(X[0], X[1]) + f(X[0], X[1] - h)) / (h**2)
    Hess[0][1] = (
        f(X[0] + h, X[1] + h)
        - f(X[0] + h, X[1] - h)
        - f(X[0] - h, X[1] + h)
        + f(X[0] - h, X[1] - h)
    ) / (h**2 * 4)
    Hess[1][0] = Hess[0][1]
    return Hess


def Hess_inv(oldGrad, newGrad, X_old, X):
    return np.matmul((newGrad - oldGrad), np.linalg.inv(X - X_old))


class opti_problem:
    def __init__(self, f, grad=None) -> float:
        self.f = f
        self.grad = None
        self.oldGrad = None
        # för jacobi matris så är input en lista med funktioner.
        # vi optimerar för tillfället bara en funktion med variabler x_0, x_1, x_2 o.s.v input X
        sig = signature(f)
        nbr_of_arguments = len(sig.parameters)

        self.X = (
            np.random.rand(nbr_of_arguments) * 2
        )  ## Inital guess, we can make this 10 and pick the best one.
        plt.plot(self.X[0], self.X[1], "ob")
        self.X_old = self.X

    def solve(self):
        iter = 0
        while self.f(self.X[0], self.X[1]) > tol and iter < 1000 or iter < 8:
            print(self.X)

            self.step()
            plt.plot(
                [self.X[0], self.X_old[0]],
                [self.X[1], self.X_old[1]],
                "k",
                linestyle="dashed",
            )
            iter += 1
        print(self.f(self.X[0], self.X[1]))

    def step(self):
        self.oldGrad = self.grad
        self.grad = grad(self.X, self.f)
        s = -np.matmul(np.linalg.inv(Hess(self.X, self.f)), self.grad)

        self.X_old = self.X
        alpha = 1

        temp_X = self.X
        temp_X = temp_X + alpha * s

        while self.f(temp_X[0], temp_X[1]) > self.f(
            self.X[0], self.X[1]
        ) + myu * alpha * np.dot(grad(self.X, self.f).T, s):
            alpha = alpha * 0.5
            temp_X = self.X + alpha * s

        self.X = temp_X
        plt.plot(self.X[0], self.X[1], "xb")


def main():
    problem = opti_problem(rosenbrock)
    problem.solve()
    plt.plot(1, 1, "or")  # plot optimal point
    plt.show()


main()
