import numpy as np
import matplotlib.pyplot as plt
from chebyquad_problem import *
from scipy.optimize import fmin_bfgs

import cProfile


TOLERANCE = 0.00001
MAX_ITER = 8000
ALPHA_UPPER = 2
MU = 0.001
MU2 = 0.01
h = 0.000001


def square(x):
    return x * x


def rosenbrock(x):
    return 100 * square(x[1] - square(x[0])) + square(1 - x[0])


def firstDerivativeV2(f, i, X):
    Xf, Xb = np.copy(X), np.copy(X)  # X-forward X-backward
    Xf[i] = Xf[i] + h
    Xb[i] = Xb[i] - h
    return (f(Xf) - f(Xb)) / (2 * h)


def secoundDerivative(f, x_index, input):
    input1, input2 = np.copy(input), np.copy(input)
    input1[x_index], input2[x_index] = input1[x_index] - h, input2[x_index] + h
    res = f(input2) - 2 * f(input) + f(input1)
    return res / pow(h, 2)


def partial(f, x_index, y_index, input):
    k = h
    input1, input2, input3, input4 = (
        np.copy(input),
        np.copy(input),
        np.copy(input),
        np.copy(input),
    )
    input1[x_index], input1[y_index] = input1[x_index] + h, input1[y_index] + k  # 1 1
    input2[x_index], input2[y_index] = input2[x_index] + h, input2[y_index] - k  # 1 -1
    input3[x_index], input3[y_index] = input3[x_index] - h, input3[y_index] + k  # -1 1
    input4[x_index], input4[y_index] = input4[x_index] - h, input4[y_index] - k  # -1 -1
    res = f(input1) - f(input2) - f(input3) + f(input4)
    return res / (4 * h * k)


def getHexian(f, x):
    dim = len(x)
    H = np.zeros([dim, dim])
    for i in range(dim):
        for j in range(dim):
            if i == j:
                H[i][j] = secoundDerivative(f, i, x)
            else:
                H[i][j] = partial(f, i, j, x)
    return H


def getGradient(f, x):
    dim = len(x)
    g2 = np.zeros(dim).T
    for i in range(dim):
        g2[i] = firstDerivativeV2(f, i, x)
    return g2


def func1(x):
    return (
        5.0
        - pow(x[1] - 4.0, 4)
        - pow(x[0] - 4.0, 3)
        + pow(x[1] - 4.0, 2)
        - pow(x[0] - 4.0, 2)
        + x[0] * 0.5
        + x[1] * x[0] * 2.0 * x[0]
    )


def sig(f):
    if f is func1:
        return 2
    elif f is rosenbrock:
        return 2
    elif False:
        pass
    else:
        return -1


def exact_line(f, x, step):  ## step is s in x = x + alpha * s
    a = 0
    a_lower = 0
    a_upper = ALPHA_UPPER

    for j in range(100):
        a = (a_lower + a_upper) / 2
        r = getGradient(f, (x - a * step))

        if np.linalg.norm(r, 2) < TOLERANCE:
            return a
        elif sum(r) < 0:
            a_upper = a
        else:
            a_lower = a
    return 1


def inexact_line(f, x, step):  ## step is s in x = x + alpha * s
    d = -step
    alpha = 1
    temp = x + alpha*d
    

    while f(temp) <= (f(x) + (MU * alpha * np.dot(getGradient(f, x).T, d))): ## Fixed
        if np.dot(getGradient(f, temp).T, d) < MU2 * np.dot(getGradient(f, x).T, d): ## Om grad på nästa punkt är typ 0 break
            break
        alpha = alpha * 0.5
        temp = x + alpha * d

    return alpha


def classicalNewton(f, X, quasi_method=None):
    gissningar_tjoflöjt = [X]
    d = None
    hess = None
    x_old = None

    for i in range(1000):
        d_old = d
        d = getGradient(f, X)
        hess_old = hess
        
        if (i != 0) and (quasi_method is not None):
            hess = quasi_method(hess_old, x_old, X, d, d_old)
        else:
            hess = np.linalg.inv(getHexian(f, X))
            
        s = np.matmul(hess, d)
        stepsize = inexact_line(f, X, s)   ## kan välja mellan exakt och ickeexakt line search. samma argument..
        x_old = X
        X = X - stepsize * s
        gissningar_tjoflöjt.append(X)
        if (np.linalg.norm((stepsize * s), 2) < TOLERANCE) and (
            np.linalg.norm(d, 2) < TOLERANCE
        ):
            break
            
    return gissningar_tjoflöjt


def plot_graph(tjohopp_returvärden):
    rosenbrockfunction = lambda x, y: (1 - x) ** 2 + 100 * (y - x**2) ** 2
    nbrOfIter = len(tjohopp_returvärden)
    print("Nbr of iteration: " + str(nbrOfIter))

    n = 100  # number of discretization points along the x-axis
    m = 100  # number of discretization points along the x-axis
    a = -0.5
    b = 2.0  # extreme points in the x-axis
    c = -1.5
    d = 4.0  # extreme points in the y-axis

    X, Y = np.meshgrid(np.linspace(a, b, n), np.linspace(c, d, m))

    Z = rosenbrockfunction(X, Y)

    ppp = plt.contour(X, Y, Z, np.logspace(-0.5, 3.5, 8, base=10), cmap="gray")
    plt.clabel(ppp, inline=True, fontsize=8)

    plt.title("Rosenbrock Function:  f(x,y)=(1-x)^2+100(y-x^2)^2$")
    plt.xlabel("x")
    plt.ylabel("y")
    plt.rc("text", usetex=True)
    plt.rc("font", family="serif")

    for i in range(len(tjohopp_returvärden)):
        if i == 0:
            plt.plot(tjohopp_returvärden[i][0], tjohopp_returvärden[i][1], "ob")
            continue
        plt.plot(tjohopp_returvärden[i][0], tjohopp_returvärden[i][1], "xb")

    plt.plot(1, 1, "or")  # plot optimal point


def BFGS(J_old, X_old, X_new, new_grad, old_grad):
    deltaX = X_new - X_old
    Grad = new_grad - old_grad
    numerator = deltaX - np.matmul(J_old, Grad)

    factor1 = 1 + np.matmul(np.matmul(Grad.T, J_old), Grad) / np.matmul(deltaX.T, Grad)
    factor2 = np.matmul(deltaX, deltaX.T) / np.matmul(deltaX.T, Grad)

    factor31 = np.matmul(deltaX, Grad.T) * J_old

    factor32 = np.matmul(np.matmul(J_old, Grad), deltaX.T)
    factor3 = (factor31 + factor32) / np.matmul(deltaX.T, Grad)

    J_new = J_old + factor1 * factor2 - factor3

    return J_new


def DFP(J_old, X_old, X_new, new_grad, old_grad):
    deltaX = X_new - X_old
    Grad = new_grad - old_grad
    numerator = deltaX - np.matmul(J_old, Grad)

    factor1 = 1 + np.matmul(np.matmul(Grad.T, J_old), Grad) / np.matmul(deltaX.T, Grad)
    factor2 = np.matmul(deltaX, deltaX.T) / np.matmul(deltaX.T, Grad)

    factor31 = np.matmul(deltaX, Grad.T) * J_old
    factor32 = np.matmul(np.matmul(J_old, Grad), deltaX.T)
    factor3 = (factor31 + factor32) / np.matmul(deltaX.T, Grad)

    J_new = J_old + factor1 * factor2 - factor3

    return J_new


def ShermanMorrisonRank1(J_old, X_old, X_new, new_grad, old_grad):
    deltaX = X_new - X_old
    Grad = new_grad - old_grad
    numerator = deltaX - np.matmul(J_old, Grad)

    denominator = np.matmul(np.matmul(deltaX.T, J_old), Grad)
    div = numerator / denominator
    J_new = J_old + div * np.matmul(deltaX.T, J_old)
    return J_new


def Symmetric(J_old, X_old, X_new, new_grad, old_grad):
    Grad = new_grad - old_grad
    deltaX = X_new - X_old
    numerator = Grad - np.matmul(J_old, deltaX)
    numerator = np.matmul(numerator, numerator.T)

    tmp1 = (Grad - np.matmul(J_old, deltaX)).T
    denominator = np.matmul(tmp1, deltaX)
    div = numerator / denominator
    J_new = J_old + div
    return J_new


def BadBroyden(J_old, X_old, X_new, new_grad, old_grad):
    deltaX = X_new - X_old
    Grad = new_grad - old_grad
    # Grad2=f(X_new)-f(x_old)
    numerator = deltaX - np.matmul(J_old, Grad)

    div = numerator / pow(np.linalg.norm(Grad), 2)
    J_new = J_old + div * Grad.T
    return J_new


def main():
    nbr_of_arguments = sig(
        rosenbrock
    )  # ger antalet parametrar en funktion har, inte längden på vektor
    x_test = np.random.rand(nbr_of_arguments) * 2
    tjohopp_returvärden = classicalNewton(
        rosenbrock, x_test
    )  ## kan ange quasi newton metod som tredje argument till classicalNewton för o köra den

    plot_graph(tjohopp_returvärden)
    print(
        "svar:", tjohopp_returvärden[len(tjohopp_returvärden) - 1]
    )  
    plt.show()

  

def main2():
    X_inputs = [[]] * 3
    X4 = np.random.rand(4)
    X8 = np.random.rand(8)
    X11 = np.random.rand(11)
    X_inputs[0] = X4
    X_inputs[1] = X8
    X_inputs[2] = X11

    sol = fmin_bfgs(chebyquad, X_inputs[2])
    tjohopp_returvärden = classicalNewton(chebyquad, X_inputs[2])


    print(sol)
    print(tjohopp_returvärden[len(tjohopp_returvärden) - 1])
    print(
        sol - tjohopp_returvärden[len(tjohopp_returvärden) - 1]
    )  ## Diff mellan optimala x
    print(tjohopp_returvärden - sol)
    print(len(tjohopp_returvärden))

## main2 för chebyquadproblemet, main för newton- och quasimetoderna
main()
