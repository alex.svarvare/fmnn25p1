import numpy as np
import gzip
import pickle
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import sys
np.set_printoptions(threshold=sys.maxsize)
 
#Load data
 
with gzip.open('mnist.pkl.gz', 'rb') as f:
    data = pickle._Unpickler(f)
    data.encoding = 'latin1'
    train, valid, test = data.load()
 
# x_train is a 50000 vector where each index contains a 1,784 vector that is the image.
# y_train is a 50000 vector where each index contains the integer which is the answer to the image
x_train, y_train = train
x_valid, y_valid = valid
x_test, y_test = test
 
# Define functions
 
def sig_f(x):
    return 1/(1+np.exp(-x))
 
def sig_deriv(x):
    return sig_f(x)*(1-sig_f(x))
 
def getModel(n_input, n_hidden, n_output):
    return [np.zeros((n_input, n_hidden)),np.random.randn(n_input,n_hidden)*0.01,np.random.randn(n_hidden, n_output)*0.01,np.zeros((n_output, n_output))]
# Define structs/objects
 
# Creating answer vector. Ex. 7 is [0, 0, 0, 0, 0, 0 ,0 ,1 ,0 ,0]
y_train_res = np.zeros((len(y_train), 10))
for i in range(len(y_train)):
    col_Index = y_train[i]
    y_train_res[i,col_Index] = 1
 
# print(y_train_res) Check answer cheet correctly formated print(len(x_train[0]))
def backpropagation(X,Y,w):
    o = [[]]*len(w)
    a = [[]]*len(w)
    o[0] = X # input layer output 
    #Forward
    for t in range(len(w)-1): 
        a[t+1] = np.matmul(o[t],w[t+1])
        o[t+1] = sig_f(a[t+1])
    #Backward

    
    delta = [[]] * len(w)
    
    delta[len(w)-1] = o[len(w)-1] - Y

    for t in reversed(range(1,len(w)-1)):
        print(t)
        print(delta[t+1].shape)
        print(w[t+1].T.shape)
        print(sig_deriv(a[t+1]).shape)

        first = np.matmul(sig_deriv(a[t+1]),w[t].T)
        res = np.matmul(first.T,delta[t+1])
        delta[t] = res


def SGD(nbr_iter, mini_batch_size):
    for tau in range(0,nbr_iter):
        model = getModel(len(x_train[0]), 30+1, 10)
        #for t in range(0,3):
            #print(w[t].shape)
        x_vec = [[]]*mini_batch_size
        y_vec = [[]]*mini_batch_size
        for i in range(mini_batch_size):
            integ = np.random.randint(0,len(x_train))
            x_vec[i] = x_train[integ]
            y_vec[i] = y_train_res[integ]
        gradient = backpropagation(x_vec,y_vec,model)
        pass

SGD(1, 3)