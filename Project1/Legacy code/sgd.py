import numpy as np
from matplotlib.pyplot import *
from BackPropagation import *
import random


def Sigmoid_forward(x):
    return 1/(1+np.exp(-x))


def Sigmoid_backward(x):
    return Sigmoid_forward(x)*(1 - Sigmoid_forward(x))


class sgd():

    def __init__(self, x, y, graph, stepSize, numberOfIterations=1000, lamb=0.01):
        self.x = x
        self.y = y
        self.graph = graph
        self.numberOfIterations = numberOfIterations
        self.stepSize = stepSize
        self.lamb = lamb
        self.w = graph.getWeight()

    def stochasticGradientDescent(self,y_valid,x_valid):
        x_rolling = self.x.copy()
        y_rolling = self.y.copy()
        w = self.w
        steps=0.01
        graf=self.graph
        m=len(y_rolling)
        batch_size=2
        bestRes=0
        best_w=w
       # self.lamb=0.01
        for k in range(batch_size):
            x_rolling = self.x.copy()
            y_rolling = self.y.copy()

            for i in range(len(y_rolling)):
                index = random.randint(0,len(y_rolling)-1)
                #print(y_rolling)
                gradient = backpropagation(
                    x_rolling[index], y_rolling[index], self.w, Sigmoid_forward, Sigmoid_backward)
                w[0] = w[0] - (1/((1+i)))*(gradient[0] + self.lamb*w[0])
                w[1] = w[1] - (1/((1+i)))*(gradient[1] + self.lamb*w[1])
                w[2] = w[2] #- steps*(gradient[2] + self.lamb*w[2])
                x_rolling=np.delete(x_rolling, index,0)
                y_rolling=np.delete(y_rolling, index,0)
 

                self.w=w
        print(w[1][1])
        print("ovan var för SGD")
        return self.w
