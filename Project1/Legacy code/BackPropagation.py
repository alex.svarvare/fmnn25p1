import numpy as np
from graf import *
#
#
#


# w i detta fall ska vara  w=[viktMatris lager0,viktmatris lager 1 ... viktmatris lager sista=detta skall vara fullt med 0or!]
# X och Y träningsdata
# activationFunction: sig-funktionen
# activationFunctionprim: derivata sig-funktionen
# OBS för W vektorn: för sista lagret skall den vara 0
def backpropagation(X, Y, w, activationFunction, activationFunctionPrim):
    T = len(w)
    O = [[]]*T
    a = [[]]*T  
    O[0] = X
    a[0] = [0]*len(w[0])
    #print(len(O[0]),len(w),len(w[0].T),len(w[1].T),len(w[2].T))
    for t in range(1, T):
        kt = len(w[t].T)
        #print(w[t],kt,t)
        O[t] = [0]*kt
        a[t] = [0]*kt
        for i in range(0, kt):
           
            #a[t][i] = weightSum(t, i, len(w[t-1]), w, O)
            
            result_sum = 0
            for j in range(0, len(w[t-1].T)):
                result_sum = result_sum+w[t-1][i][j]*O[t-1][j]
                #print(result_sum)
            #print(result_sum,"hej  ",kt,t)
            a[t][i]=result_sum



            O[t][i] = f(a[t][i])
            #print(O[0][i],i)
    delta = [[]]*T

    #print(a[1])

    resultL = []
    for i in range(len(O[T-1])):
        #print("y",Y[i])
        #print(O[T-1][i],"o")
        resultL.append(O[T-1][i] - Y[i])

  

    delta[T-1] = resultL#O[T-1]-Y
    
   
    for t in reversed(range(0, T-1)):
        kt = len(w[t].T)  
        
        delta[t] = [0]*kt
        for i in range(0, kt):
            #delta[t][i] = deltaSum(len(w[t+1]), t, i, a, fp, delta, w)  
            sum_result = 0
            for j in range(0, len(w[t+1].T)):
                sum_result =sum_result+ w[t][j][i]*delta[t+1][j]*fp(a[t+1][j])
            delta[t][i]=sum_result
              


    
    PartialDerivative = [[]]*T
    PartialDerivative = w.copy()
    #print(PartialDerivative)

    for t in range(0, T-1):
        current_layer_width = len(w[t].T)  # tmp
        next_layer_width = len(w[t+1].T)
        for i in range(0, next_layer_width):
            for j in range(0, current_layer_width):    
                #print("____________________")  
                #print(delta[t+1][i]) 
                #print(fp(a[t+1][i]))
                #print(O[t][j])  
                #print(j)
                PartialDerivative[t][i][j] = delta[t+1][i] * fp(a[t+1][i])*O[t][j]
    #print(PartialDerivative[1])
    return PartialDerivative

#

def weightSum(t, i, kt, w, O):
    result = 0
    for j in range(0, kt):
        result = result+w[t-1][i][j]*O[t-1][j]
    return result


def deltaSum(kt, t, i, a, func, delta, w):
    result = 0
    for j in range(0, kt):
        result =result+ w[t][j][i]*delta[t+1][j]*func(a[t+1][j])
    return result



def f(input):
    return 1/(1+np.exp(-input))


def fp(input):
    return f(input)*(1-f(input))

#X= np.random.rand(20,784)*10
#Y=np.random.rand(10)
#graf=graph(784,10,30,30)
#w_g=graf.getWeight()

#print()
#res=backpropagation(X[0],Y,w_g,f,fp)
#print(res)
