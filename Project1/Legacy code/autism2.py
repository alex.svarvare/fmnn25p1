import numpy as np
import gzip
import pickle
import sys
import random
from main import *

# np.set_printoptions(threshold=sys.maxsize)

# with gzip.open('mnist.pkl.gz', 'rb') as f:
#     data = pickle._Unpickler(f)
#     data.encoding = 'latin1'
#     train, valid, test = data.load()

# x_train, y_train2 = train
# x_valid, y_valid = valid
# x_test, y_test = test


# # transformerar test y från en inter till en array med 10 element
# y_train = np.zeros((len(y_train2), 10))
# for i in range(0, len(y_train2)):
#     y_train[i] = [0]*10
#     a = y_train2[i]
#     y_train[i][a] = 1


def sigmoid_func(x):
    return 1.0/(1.0+np.exp(-x))


def sigmoid_deriv_func(x):
    return sigmoid_func(x)*(1.0 - sigmoid_func(x))

# return weights w where
# wtij  :  weight from edge v(t-1) i to v(t) j


class FNN:
    def __init__(self, inputLayer_width, hiddenLayer_width, outputLayer_width):
        self.model = [np.zeros((inputLayer_width, inputLayer_width)), np.random.randn(inputLayer_width, hiddenLayer_width)
                      * 0.01, np.random.randn(hiddenLayer_width, outputLayer_width)*0.01, np.zeros((outputLayer_width, outputLayer_width))]

    def pred2(self, X):
        T = 3
        # X=np.append(X,10.0)
        X = np.append(X, 1.0)
        Input_a = [[]]*T
        output_o = [[]]*T
        output_o[0] = X
        for t in range(1, T):
            width_current_layer = len(self.model[t].T)
            output_o[t] = [0.0]*width_current_layer
            Input_a[t] = [0.0]*width_current_layer

            for j in range(0, width_current_layer):

                Input_a[t][j] = np.dot(output_o[t-1], (self.model[t].T)[j])
                output_o[t][j] = sigmoid_func(Input_a[t][j])
            if t == 1:
                Input_a[t][width_current_layer-1] = 1
                output_o[t][width_current_layer -
                            1] = sigmoid_func(Input_a[t][width_current_layer-1])

        return np.exp(Input_a[T-1])/np.sum(np.exp(Input_a[T-1]))

    # Input_a[t][j] : input to node j in layer t f : t=1,2..T   j=1 ... layer width
    # output_o[t][j] : output from node j with activationfunction applied

    def backpropagation(self, X, Y):
        T = 3
        Input_a = [[]]*T
        output_o = [[]]*T
        output_o[0] = X
        for t in range(1, T):
            width_current_layer = len(self.model[t].T)
            output_o[t] = [0.0]*width_current_layer
            Input_a[t] = [0.0]*width_current_layer

            for j in range(0, width_current_layer):
                sum_a = 0
                for r in range(0, len(self.model[t-1].T)):
                    sum_a = sum_a+self.model[t][r][j]*output_o[t-1][r]

                Input_a[t][j] = sum_a
                output_o[t][j] = sigmoid_func(Input_a[t][j])
            if t == 1:
                Input_a[t][width_current_layer-1] = 1
                output_o[t][width_current_layer -
                            1] = sigmoid_func(Input_a[t][width_current_layer-1])

        delta = [[]]*T

        # np.exp(Input_a[T-1])/np.sum(np.exp(Input_a[T-1]))-Y
        delta[T-1] = (output_o[T-1]-Y)

        for t in reversed(range(1, T-1)):
            next_layer = len(self.model[t].T)
            delta[t] = [0.0]*next_layer
            for j in range(0, next_layer):
                delta_sum = 0
                for i in range(0, len(self.model[t+1].T)):
                    delta_sum = delta_sum+delta[t+1][i] * \
                        sigmoid_deriv_func(
                            Input_a[t+1][i])*self.model[t+1][j][i]
                delta[t][j] = delta_sum

        gradient = self.model.copy()
        gradient[1] = gradient[1]*0
        gradient[2] = gradient[2]*0
        # print(gradient[2])
        for t in range(1, T):
            for r in range(0, len(output_o[t-1])):
                for j in range(0, len(Input_a[t])):
                    # print(t,r,j)

                    gradient[t][r][j] = delta[t][j] * \
                        sigmoid_deriv_func(Input_a[t][j])*output_o[t-1][r]

        return gradient

    def train(self, x, y, x_test, y_test, epochs, batch, learnrate):
        filis = fil()
        stats = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for epoch_nbr in range(epochs):
            x_batch, y_batch = x.copy(), y.copy()
            # print("new epoch:" ,epoch_nbr)
            for i in range(len(y_batch)):
                index = random.randint(0, len(y_batch)-1)
                x_biased = np.append(x_batch[index], 1.0)
                gradient = self.backpropagation(x_biased, y_batch[index])
                stats[np.array(y_batch[index]).argmax()] += 1

                # TURN ON/OFF OLD WEIGHTS
                if (i == 0):
                    gammal = filis.get()
                    self.model[1] = gammal[0]
                    self.model[2] = gammal[1]

                self.model[1] -= (learnrate)/(1) * \
                    gradient[1]  # + lambda_s*w[1])
                self.model[2] -= (learnrate)/(1) * \
                    gradient[2]  # + lambda_s*w[2])

                x_batch = np.delete(x_batch, index, 0)
                y_batch = np.delete(y_batch, index, 0)
                if i % 200 == 0:
                    print("itteration i=", i)
                    self.validate(x_test, y_test, 100)
                    filis.save(self.model[1], self.model[2])

            if epoch_nbr % 1 == 1000:
                print("i: ", epoch_nbr)
                print(stats)
                print("")
                for z in range(len(y_test[0:5])):
                    # pred=predict([w[1],w[2]],x_test[z])
                    pred = pred2(w, x_test[z])
                    print(np.array(pred).argmax(), pred)
                    print(y_test[z])
                    print()

    def SGD2(self, x, y, w, epochs, learnrate, x_test, y_test, batch_size):
        # batch_size=0
        for epoch_nbr in range(epochs):
            randomize = np.arange(len(y))
            np.random.shuffle(randomize)
            x = x[randomize]
            y = y[randomize]
            x_batch = len(x,)
            gradient = w*0
            Useless_index = 0
            for m in range(0, len(y), batch_size):
                Useless_index += 1
                y_use = y[m:m+batch_size]
                x_use = x[m:m+batch_size]
                gradient = gradient*0
                current_batch_size = len(y_use)
                for i in range((current_batch_size)):
                    x_biased = np.append(x_use[i], 1.0)
                    gradient = gradient + \
                        self.backpropagation(x_biased, y_use[i], w)
                w[1] = w[1]-(learnrate)*gradient[1] / \
                    current_batch_size  # + lambda_s*w[1])
                w[2] = w[2]-(learnrate)*gradient[2]/current_batch_size
                learnrate = learnrate

                if Useless_index % 100 == 0:
                    print(epoch_nbr)
                    self.validate(x_test, y_test, w, len(y_test))
        return w

        # for z in range(len(y_test)):
        #   #pred=predict([w[1],w[2]],x_test[z])
        #  pred=pred2(w,x_test[z])
        #  print(np.array(pred).argmax(),pred)
        #  print(y_test[z])
        # print()
        # return w

    def test(self, x_test, y_test):
        # TODO: Implement a forward pass of the
        # x_test, compar and calculat accuracy
        return 0.01

    def validate(self, x, y, nbr_validation):

        stats = np.zeros((10, 12))
        result = 0
        for z in range(nbr_validation):
            index = random.randint(0, len(y)-1)
            x_rand, y_rand = x[index], y[index]
            x, y = np.delete(x, index, 0), np.delete(y, index, 0)
            predicted = self.pred2(x_rand)
            predicted_val = np.array(predicted).argmax()
            if predicted_val == y_rand:
                result += 1
                stats[y_rand][predicted_val] += 1
            else:
                stats[y_rand][predicted_val] -= 1
            stats[y_rand][10] += 1
            stats[predicted_val][11] += 1
        print("_________________________________")
        print("testet data :")
        print(stats)
        print("rows: correct y")
        print("cols : guessed y if wrong")
        print("last element for each row: total nbr of value row")
        print("RESULT:")
        print("nbr correct guesses: ", result, ", out of ", nbr_validation)
        print()
        print("RESULT:", result/nbr_validation)
        print("________________________________-")

    # idx = np.random.choice(np.arange(len(y_train)), 1000, replace=False)
    # x_train = x_train[idx]
    # y_train = y_train[idx]

    # model = getModel(785, 30+1, 10)
    # ww = SGD(x_train, y_train, model, 1000000, 0.01, x_valid, y_valid)

    # fitted_w=stochasticGradientDescent(y_train[0:10],x_train[0:10],graf,0.01,2000000000,y_train[0:10],x_train[0:10])


# idx = np.random.choice(np.arange(len(y_train)), 1000, replace=False)
# x_train = x_train[idx]
# y_train = y_train[idx]


# model=getModel(785,30+1,10)
# ww=SGD(x_train,y_train,model,1000000,0.01,x_valid,y_valid)


# fitted_w=stochasticGradientDescent(y_train[0:10],x_train[0:10],graf,0.01,2000000000,y_train[0:10],x_train[0:10])

# fitted_w=[fitted_w[1],fitted_w[2]]
# eval_result = 0
# y_valid,x_valid=y_valid[0:500],x_valid[0:500]
# print(fitted_w[1])
# y_valid,x_valid=y_train[0:3],x_train[0:3]
# y_valid,x_valid=y_test,x_test

# for i in range(0, len(y_valid)):
   # pred=predict(fitted_w,x_valid[i])
   # print("max : ",pred,"acctual:",y_valid[i])
   # if pred == y_valid[i]:
    #    eval_result += 1
# result_valid_data = eval_result/len(y_valid)


# print(result_valid_data)
