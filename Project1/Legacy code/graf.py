import numpy as np


# ett lager.
# step kommer leda till en "dot" product, oklart om den ska vara transponerat
class layer:
    def __init__(self,nbrNeurons,nbrOutputs, activationFunc=None) -> None:
        self.weights = np.random.rand(nbrNeurons, nbrOutputs).T
        self.activationFunction = None
        self.output=None
        if activationFunc is not None:
            self.activationFunction = activationFunc
        self.bias = 1

    def step(self, input):
        if self.activationFunction is None:
            
            self.output = np.dot(input, self.weights)
        else:
            f2=list(map(self.activationFunction, input))
            #print(f2)
            self.output = np.dot(
                f2, self.weights.T)
            
            #self.output=list(map(self.activationFunction, self.output))
        pass

    def setLayerweight(self, w):
        self.weights = w


def sigFunction(input):
    return 1/(1+np.exp(-input))


class graph:
    def __init__(self, nbrInput, nbrOutput, NbrHiddenNeurons, width_hidden) -> None:
        self.InputLayer = layer(nbrInput, width_hidden,
                                activationFunc=sigFunction)
        # asså denna är ju helt värdelös lol
        self.OutputLayer = layer(
            nbrOutput, nbrOutput, activationFunc=sigFunction)
        self.hiddenLayer = layer(
            width_hidden, nbrOutput, activationFunc=sigFunction)

    def predict(self, Input):
        self.InputLayer.step(Input)
        self.hiddenLayer.step(self.InputLayer.output)
 
        result = list(map(sigFunction, self.hiddenLayer.output))
        # däremot är index_för_max(med activationfunction)=index_för_max(utan activationfcuntion) så det kvittar
        #print(result)

        return result

    def getWeight(self):
        return [self.InputLayer.weights, self.hiddenLayer.weights, self.OutputLayer.weights*0]

    def setWeight(self, newWeight):
        self.InputLayer.setLayerweight(newWeight[0])
        self.hiddenLayer.setLayerweight(newWeight[1])
