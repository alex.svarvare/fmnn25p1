import numpy as np
import gzip
import pickle
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import sys
from graf import *
from sgd import *
np.set_printoptions(threshold=sys.maxsize)


def Sigmoid_forward(x):
    return 1/(1+np.exp(-x))


def Sigmoid_backward(x):
    return Sigmoid_forward(x)*(1 - Sigmoid_forward(x))


with gzip.open('mnist.pkl.gz', 'rb') as f:
    data = pickle._Unpickler(f)
    data.encoding = 'latin1'
    train, valid, test = data.load()

x_train, y_train2 = train
x_valid, y_valid = valid
x_test, y_test = test


graf = graph(784, 10, 30, 30)

y_train=np.zeros((len(y_train2),10))
for i in range(0,len(y_train2)):
    y_train[i]=[0]*10
    a=y_train2[i]
    y_train[i][a]=1
    #print( y_train[i].index(max( y_train[i])),a)
   
  

# snabbare
x_train_snell=x_train[0:100]
y_train_snell=y_train[0:100]

sgd = sgd(x_train_snell, y_train_snell, graf, stepSize=0.01,
          numberOfIterations=1000, lamb=0)



weights=sgd.stochasticGradientDescent(y_valid[1:500],x_valid[1:500])
#w_input=weights[0]
w_hidden=weights
#weights[0]=vikter för input
#weights[1]=vikter för hidden
#print(weights[0])
#print()
#print(weights[1])
graf.setWeight(weights)



eval_result = 0
y_valid=y_valid[0:100]
x_valid=x_valid[0:100]
for i in range(0, len(y_valid)):
    prediction = graf.predict(x_valid[i])
    #print("prediction : ", prediction)
    pred = prediction.index(max(prediction))
    print(pred,y_valid[i])
    if pred == y_valid[i]:
        eval_result += 1
result_valid_data = eval_result/len(y_valid)


print(result_valid_data)
