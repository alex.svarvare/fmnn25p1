import numpy as np
import gzip
import pickle
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import sys
import random

np.set_printoptions(threshold=sys.maxsize)

def Sigmoid(x):
    return 1.0/(1.0+np.exp(-x))


def SigmoidPrim(x):
    return Sigmoid(x)*(1.0 - Sigmoid(x))


with gzip.open('mnist.pkl.gz', 'rb') as f:
    data = pickle._Unpickler(f)
    data.encoding = 'latin1'
    train, valid, test = data.load()

x_train, y_train2 = train
x_valid, y_valid = valid
x_test, y_test = test



# transformerar test y från en inter till en array med 10 element 
y_train=np.zeros((len(y_train2),10))
for i in range(0,len(y_train2)):
    y_train[i]=[0]*10
    a=y_train2[i]
    y_train[i][a]=1
    #print(a,"==",y_train[i])
   
  
# snabbare
x_train_snell=x_train[0:1000]
y_train_snell=y_train[0:1000]





def predict(w,input):
    sigmoid_vector=np.vectorize(Sigmoid)
   
    input_to_hidden=np.dot(w[0].T,sigmoid_vector(input))
    hidden_to_output=np.dot(w[1].T,sigmoid_vector(input_to_hidden))
    result=np.array(hidden_to_output).argmax()
    #print(input_to_hidden)
    #print("__________________--")
    #print(sum(input_to_hidden))
    #print()
    #print(w[1][0])
    #print()
 
    #print(hidden_to_output)
    #result=sigmoid_vector(hidden_to_output)
    return result



def backpropagation(X, Y, w, f, fp):
    T = len(w)
    O = [[]]*T
    a = [[]]*T  
    print("--------------",T)
    O[0] = X
    for t in range(1, T):
        kt = len(w[t].T)
        O[t] = [0.0]*kt
        a[t] = [0.0]*kt
        for i in range(1, kt):
            a[t][i]=np.dot(w[t-1][i],O[t-1]) # alternativt result_sum från ovan. ÄR EJ HELT SÄKER men subtraherar man dem får man typ en dif på exp(e-16)
            O[t][i] = f(a[t][i])
    delta = [[]]*T
    delta[T-1] = O[T-1]-Y
    for t in reversed(range(0, T-1)):
        kt = len(w[t].T) 
        delta[t] = [0]*kt
        for i in range(0, kt):  
            sum_result = 0
            for j in range(0, len(w[t+1].T)):
                sum_result =sum_result+ w[t][j][i]*delta[t+1][j]*fp(a[t+1][j])
            delta[t][i]=sum_result

              
    
    PartialDerivative = w.copy()


    for t in range(0, T-1):
        current_layer_width = len(w[t].T)  # tmp
        next_layer_width = len(w[t+1].T)
        for i in range(0, next_layer_width):
            for j in range(0, current_layer_width):    
                PartialDerivative[t][i][j] = delta[t+1][i] * fp(a[t+1][i])*O[t][j]
                #PartialDerivative[t][i][j] = delta[t][i] * fp(a[t][i])*O[t-1][j]
    
  
    return PartialDerivative


 

def backpropagationN(X, Y, w, f, fp):
    T = len(w)
    O = [[]]*T
    a = [[]]*T  
    O[0] = X
    for t in range(1, T):
        kt = len(w[t].T)
        O[t] = [0.0]*(kt)
        a[t] = [0.0]*(kt)
        for j in range(0, kt):
            #print(len((w[t].T)[j]),len(O[t-1]))
            a[t][j]=np.dot((w[t].T)[j].T,O[t-1]) # alternativt result_sum från ovan. ÄR EJ HELT SÄKER men subtraherar man dem får man typ en dif på exp(e-16)
           # print("atj=",a[t][i],"t=",t,"i=",i)
            #for r in range(0,len())
            O[t][j] = f(a[t][j])


    delta = [[]]*(T)
    #print(delta[T-2])
    delta[T-2] = (O[T-2]-Y)
  
    #delta[T-2]=
    print(np.array(Y).argmax()," fick in , gissar på",np.array(O[2]).argmax(),"sum för delta ",sum(map(abs, delta[T-2])))  

    



    for t in reversed(range(1, T-2)):
        kt = len(w[t+1].T)
        ktt= len(w[t].T)
        delta[t] = [0]*ktt
        #print(t,T)
        #print("     ",t,ktt,kt)
        for j in range(0, ktt):  
            sum_result = 0
            for i in range(0, kt):
                
                sum_result =sum_result+ w[t+1][j][i]*delta[t+1][i]*fp(a[t+1][i])
            delta[t][j]=sum_result

    PartialDerivative = w.copy()
    for t in range(1, T-1):
        #print(t)
        current_layer_width = len(w[t].T)  # tmp
        next_layer_width = len(w[t])
        for i in range(0, next_layer_width):
            for j in range(0, current_layer_width):    
                PartialDerivative[t][i][j] = delta[t][j] * fp(a[t][j])*O[t-1][i]
    return PartialDerivative    



def stochasticGradientDescent(y,x,w_input,lambda_s,batch_size,y_valid,x_valid):
        #lambda_s=0.01
        w = w_input
        steps=0.01
        #batch_size=20
        best_w=w
        best_result=0
        
        for k in range(batch_size):
            x_batch,y_batch = x.copy(),y.copy()
            #w=[np.random.rand(30, 784)*0.1,np.random.rand(10, 30)*0.1,np.random.rand(10, 10)*0 ]
            #print("",w[2][0])
            #print("__________________________________")
            for i in range(len(y_batch)-1):
                index = random.randint(0,len(y_batch)-1)
                #gradient = BKP_powerpoint(x_batch[index], y_batch[index], w, Sigmoid, SigmoidPrim)
                gradient = backpropagationN(x_batch[index], y_batch[index], w, Sigmoid, SigmoidPrim)
                w[1] = w[1] - 0.001*(gradient[1] )#+ lambda_s*w[1])
                w[2] = w[2] - 0.001*(gradient[2] )#+ lambda_s*w[2])
                #w[2] = w[2] - 0.0001*(gradient[2]) #- steps*(gradient[2] + self.lamb*w[2])
             

                print()
                #print("batch nbr ",k,"itteration i=",i)
                #print("trained on",y_batch[index])
                x_batch=np.delete(x_batch, index,0)
                y_batch=np.delete(y_batch, index,0)
                eval_result=0
                #print(w[2])
                #print(w[2])
                
                
                for z in range(0,len(y_valid)):
                    pred=predict([w[1],w[2]],x_valid[z])
                    #print((pred),"valid=", y_valid[z])
                    
                    #if pred == y_valid[i]:
                  #  if np.array(y_valid[z]).argmax()==pred:
                   #     eval_result += 1
                
                #current=eval_result/len(y_valid)
                #if eval_result/len(y_valid)>best_result:
                  #  best_w=[w[0].copy(),w[1].copy(),w[2].copy()]
                 #   best_result=eval_result/len(y_valid)
                #print(i,k," and res :",best_result,current)

        return w



graf=[[],np.random.rand(784,30 ),np.random.rand(30, 10),np.zeros((10, 10)) ]


idx = np.random.choice(np.arange(len(y_train)), 10000, replace=False)
x_train = x_train[idx]
y_train = y_train[idx]



fitted_w=stochasticGradientDescent(y_train[0:10],x_train[0:10],graf,0.01,2000000000,y_train[0:10],x_train[0:10])

fitted_w=[fitted_w[1],fitted_w[2]]
eval_result = 0
y_valid,x_valid=y_valid[0:500],x_valid[0:500]
print(fitted_w[1])
#y_valid,x_valid=y_train[0:3],x_train[0:3]
#y_valid,x_valid=y_test,x_test
for i in range(0, len(y_valid)):
    pred=predict(fitted_w,x_valid[i])
    print("max : ",pred,"acctual:",y_valid[i])
    if pred == y_valid[i]:
        eval_result += 1
result_valid_data = eval_result/len(y_valid)


print(result_valid_data)











